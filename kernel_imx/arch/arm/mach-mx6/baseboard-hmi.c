#include <asm/mach/arch.h>

#include <linux/clk.h>
#include <linux/i2c.h>
#include <linux/platform_device.h>
#include <linux/regulator/fixed.h>
#include <linux/regulator/machine.h>

#include <mach/common.h>
#include <mach/devices-common.h>
#include <mach/gpio.h>
#include <mach/iomux-mx6dl.h>
#include <mach/iomux-v3.h>
#include <mach/mx6.h>

#include "edm.h"

/****************************************************************************
 *                                                                          
 * SGTL5000 Audio Codec
 *                                                                          
 ****************************************************************************/

static struct regulator_consumer_supply hmibase_sgtl5000_consumer_vdda = {
	.supply		= "VDDA",
	.dev_name	= "0-000a", /* Modified load time */
};

/* ------------------------------------------------------------------------ */

static struct regulator_consumer_supply hmibase_sgtl5000_consumer_vddio = {
	.supply		= "VDDIO",
	.dev_name	= "0-000a", /* Modified load time */
};

/* ------------------------------------------------------------------------ */

static struct regulator_init_data hmibase_sgtl5000_vdda_reg_initdata = {
	.num_consumer_supplies = 1,
	.consumer_supplies = &hmibase_sgtl5000_consumer_vdda,
};

/* ------------------------------------------------------------------------ */

static struct regulator_init_data hmibase_sgtl5000_vddio_reg_initdata = {
	.num_consumer_supplies = 1,
	.consumer_supplies = &hmibase_sgtl5000_consumer_vddio,
};

/* ------------------------------------------------------------------------ */

static struct fixed_voltage_config hmibase_sgtl5000_vdda_reg_config = {
	.supply_name	= "VDDA",
	.microvolts		= 2500000,
	.gpio			= -1,
	.init_data		= &hmibase_sgtl5000_vdda_reg_initdata,
};

/* ------------------------------------------------------------------------ */

static struct fixed_voltage_config hmibase_sgtl5000_vddio_reg_config = {
	.supply_name	= "VDDIO",
	.microvolts		= 3300000,
	.gpio			= -1,
	.init_data		= &hmibase_sgtl5000_vddio_reg_initdata,
};

/* ------------------------------------------------------------------------ */

static struct platform_device hmibase_sgtl5000_vdda_reg_devices = {
	.name	= "reg-fixed-voltage",
	.id		= 0,
	.dev	= {
		.platform_data = &hmibase_sgtl5000_vdda_reg_config,
	},
};

/* ------------------------------------------------------------------------ */

static struct platform_device hmibase_sgtl5000_vddio_reg_devices = {
	.name	= "reg-fixed-voltage",
	.id		= 1,
	.dev	= {
		.platform_data = &hmibase_sgtl5000_vddio_reg_config,
	},
};

/* ------------------------------------------------------------------------ */

static struct platform_device hmibase_audio_device = {
	.name = "imx-sgtl5000",
};

/* ------------------------------------------------------------------------ */

static const struct i2c_board_info hmibase_sgtl5000_i2c_data __initdata = {
	I2C_BOARD_INFO("sgtl5000", 0x0a)
};

/* ------------------------------------------------------------------------ */

static char hmibase_sgtl5000_dev_name[8] = "0-000a";

static __init int hmibase_init_sgtl5000(void) {
	hmibase_sgtl5000_dev_name[0] = '0' + edm_i2c[1];
	hmibase_sgtl5000_consumer_vdda.dev_name = hmibase_sgtl5000_dev_name;
	hmibase_sgtl5000_consumer_vddio.dev_name = hmibase_sgtl5000_dev_name;
        
	hmibase_audio_device.dev.platform_data = (struct mxc_audio_platform_data *)edm_analog_audio_platform_data;
	platform_device_register(&hmibase_audio_device);
        
	i2c_register_board_info(edm_i2c[1], &hmibase_sgtl5000_i2c_data, 1);
	platform_device_register(&hmibase_sgtl5000_vdda_reg_devices);
	platform_device_register(&hmibase_sgtl5000_vddio_reg_devices);
	return 0;
}

/****************************************************************************
 *                                                                          
 * PRISM Touch
 *                                                                          
 ****************************************************************************/

#include <mach/gpio.h>
#include <linux/delay.h>
static struct i2c_board_info hmibase_prism_i2c_data[] = {
	{
		I2C_BOARD_INFO("prism", 0x10),
		.irq	= -EINVAL,
		.flags = I2C_CLIENT_WAKE,
	},
};

static __init int hmibase_init_prism(void) {
	unsigned prism_reset;
	unsigned prism_irq;

	prism_reset = edm_external_gpio[9];
	prism_irq = edm_external_gpio[8];

	gpio_direction_output(prism_reset, 0);
	gpio_set_value(prism_reset, 0);
	mdelay(50);
	gpio_set_value(prism_reset, 1);
	hmibase_prism_i2c_data[0].irq = gpio_to_irq(prism_irq);
	gpio_direction_input(prism_irq);
	i2c_register_board_info(edm_i2c[1], &hmibase_prism_i2c_data[0], 1);
	return 0;
}

/****************************************************************************
 *
 * GPIO_BUTTON
 *
 ****************************************************************************/
#if defined(CONFIG_KEYBOARD_GPIO) || defined(CONFIG_KEYBOARD_GPIO_MODULE)
static struct gpio_keys_button hmibase_gpio_buttons[] = {
	{
		.code                   = KEY_POWER,
		.desc                   = "btn power-key",
		.wakeup                 = 1,
		.active_low             = 1,
	}, {
		.code                   = 172,
		.desc                   = "btn home-key",
		.wakeup                 = 0,
		.active_low             = 1,
	}, {
		.code                   = 158,
		.desc                   = "btn back-key",
		.wakeup                 = 0,
		.active_low             = 1,
	}, {
		.code                   = 139,
		.desc                   = "btn menu-key",
		.wakeup                 = 0,
		.active_low             = 1,
	},
};

static struct gpio_keys_platform_data hmibase_gpio_key_info = {
	.buttons        = hmibase_gpio_buttons,
	.nbuttons       = ARRAY_SIZE(hmibase_gpio_buttons),
};

static struct platform_device hmibase_keys_gpio = {
	.name   = "gpio-keys",
	.id     = -1,
	.dev    = {
		.platform_data  = &hmibase_gpio_key_info,
	},
};

static __init int hmibase_init_gpio_keys(void)
{
	gpio_free(edm_external_gpio[0]);
	gpio_free(edm_external_gpio[4]);
	gpio_free(edm_external_gpio[5]);
	gpio_free(edm_external_gpio[6]);
	hmibase_gpio_buttons[0].gpio = edm_external_gpio[0]; // power int
	hmibase_gpio_buttons[1].gpio = edm_external_gpio[4]; // home
	hmibase_gpio_buttons[2].gpio = edm_external_gpio[5]; // back
	hmibase_gpio_buttons[3].gpio = edm_external_gpio[6]; // menu
	platform_device_register(&hmibase_keys_gpio);

	unsigned power_kill;
	power_kill = edm_external_gpio[2];

	gpio_direction_output(power_kill, 1);
	gpio_set_value(power_kill, 1);

	return 0;
}
#else
static inline __init int hmibase_init_gpio_keys(void) { return 0; }
#endif

/****************************************************************************
 *
 * EEPROM
 *
 ****************************************************************************/

#if defined(CONFIG_EEPROM_AT24) || defined(CONFIG_EEPROM_AT24_MODULE)
#include <linux/i2c/at24.h>

static void hmibase_board_setup(struct memory_accessor *mem_acc, void *context)
{
	char eeprom_read_test_string[20];
	int ret = 0;
	#define EEPROM_READ_STR_OFFSET	0x0
	ret = mem_acc->read(mem_acc, (char *)&eeprom_read_test_string,
                EEPROM_READ_STR_OFFSET, sizeof(eeprom_read_test_string));

	printk("%s:Test on Baseboard Identification.\n",__FUNCTION__);
	/*This should access content of eeprom*/
}

static struct at24_platform_data hmibase_at24c08_pdata = {
	.byte_len       = SZ_8K / 8,
	.page_size      = 16,
	.flags          = AT24_FLAG_ADDR16,
	.setup          = hmibase_board_setup,
	.context        = (void *)NULL,
};

static const struct i2c_board_info hmibase_at24c08__binfo = {
	I2C_BOARD_INFO("24c08", 0x50),
	.platform_data  = &hmibase_at24c08_pdata,
};

static __init int hmibase_init_eeprom(void) {
	i2c_register_board_info(edm_i2c[2], &hmibase_at24c08__binfo, 1);
	return 0;
}
#else
static inline __init int hmibase_init_eeprom(void) { return 0; }
#endif

/****************************************************************************
 *                                                                          
 * main-function for hmi baseboard
 *                                                                          
 ****************************************************************************/

static __init int hmibase_init(void) {
	int ret = 0;
	ret += hmibase_init_sgtl5000();
	ret += hmibase_init_prism();
	ret += hmibase_init_gpio_keys();
	ret += hmibase_init_eeprom();
	return ret;
}
subsys_initcall(hmibase_init);

static __exit void hmibase_exit(void) {
	/* Actually, this cannot be unloaded. Or loaded as a module..? */
} 
module_exit(hmibase_exit);

MODULE_DESCRIPTION("HMI baseboard driver");
MODULE_AUTHOR("Hunt Lin <hunt.lin@touchrev.com>");
MODULE_LICENSE("GPL");
