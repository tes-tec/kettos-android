#ifndef WANDBOARD_ANDROID_QUAD_H
#define WANDBOARD_ANDROID_QUAD_H

/*********Solo***************/
#if 0
 #define CONFIG_MX6DL
 #define CONFIG_MX6SOLO_DDR3
 #define CONFIG_DDR_32BIT
#endif
/*********DualLite***********/
#if 0
 #define CONFIG_MX6DL
 #define CONFIG_MX6DL_DDR3
 #define CONFIG_DDR_64BIT
#endif
/*********QUAD***************/
#if 1
 #define CONFIG_MX6Q
/* #define CONFIG_CMD_SATA */
#endif
/****************************/

#include "wandboard_android.h"

#endif
