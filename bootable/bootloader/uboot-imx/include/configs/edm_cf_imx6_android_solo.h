#ifndef EDM_CF_IMX6_ANDROID_SOLO_H
#define EDM_CF_IMX6_ANDROID_SOLO_H

/*********Solo***************/
#if 1
 #define CONFIG_MX6DL
 #define CONFIG_MX6SOLO_DDR3
 #define CONFIG_DDR_32BIT
#endif
/*********DualLite***********/
#if 0
 #define CONFIG_MX6DL
 #define CONFIG_MX6DL_DDR3
 #define CONFIG_DDR_64BIT
#endif
/*********QUAD***************/
#if 0
 #define CONFIG_MX6Q
/* #define CONFIG_CMD_SATA */
#endif
/****************************/

#include "edm_cf_imx6_android.h"

#endif
