# This is a FSL Android Reference Design platform based on i.MX6Q ARD board
# It will inherit from FSL core product which in turn inherit from Google generic

$(call inherit-product, device/fsl/imx6/imx6_edm.mk)
$(call inherit-product-if-exists,vendor/google/products/gms.mk)

$(shell touch device/fsl/hmi/fstab_nand.hmi)
$(shell touch device/fsl/hmi/fstab.hmi)

# Overrides
PRODUCT_NAME := hmi
PRODUCT_DEVICE := hmi

PRODUCT_PACKAGES += SwitchCOM \
	sp \
	Music

PRODUCT_COPY_FILES += \
	device/fsl/hmi/required_hardware.xml:system/etc/permissions/required_hardware.xml \
	device/fsl/hmi/init.hmi.rc:root/init.hmi.rc \
	device/fsl/hmi/vold.fstab:system/etc/vold.fstab \
	device/fsl/hmi/gpsreset.sh:system/etc/gpsreset.sh \
	device/fsl/hmi/audio_policy.conf:system/etc/audio_policy.conf \
	device/fsl/hmi/audio_effects.conf:system/vendor/etc/audio_effects.conf

PRODUCT_COPY_FILES += \
	device/fsl/hmi/init.rc:root/init.rc \
	device/fsl/hmi/fstab.hmi:root/fstab.hmi \
	device/fsl/imx6/etc/init.usb.rc:root/init.hmi.usb.rc \
	device/fsl/hmi/ueventd.hmi.rc:root/ueventd.hmi.rc

PRODUCT_COPY_FILES += \
    device/fsl/hmi/i2c-tools/i2cdetect:system/bin/i2cdetect \
    device/fsl/hmi/i2c-tools/i2cdump:system/bin/i2cdump \
    device/fsl/hmi/i2c-tools/i2cget:system/bin/i2cget \
    device/fsl/hmi/i2c-tools/i2cset:system/bin/i2cset \
    device/fsl/hmi/tr/su0:system/xbin/su0 \
    device/fsl/hmi/tr/fw_printenv:system/xbin/fw_printenv \
    device/fsl/hmi/tr/fw_printenv:system/xbin/fw_setenv \
    device/fsl/hmi/tr/fw_env.config:system/etc/fw_env.config \
    device/fsl/hmi/packages/sp/libserial_port.so:system/vendor/lib/libserial_port.so

# GPU files

DEVICE_PACKAGE_OVERLAYS := device/fsl/hmi/overlay

PRODUCT_CHARACTERISTICS := tablet

PRODUCT_AAPT_CONFIG += xlarge large tvdpi hdpi

PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.hardware.location.gps.xml:system/etc/permissions/android.hardware.location.gps.xml \
	frameworks/native/data/etc/android.hardware.wifi.xml:system/etc/permissions/android.hardware.wifi.xml \
	frameworks/native/data/etc/android.hardware.sensor.light.xml:system/etc/permissions/android.hardware.sensor.light.xml \
	frameworks/native/data/etc/android.hardware.sensor.accelerometer.xml:system/etc/permissions/android.hardware.sensor.accelerometer.xml \
	frameworks/native/data/etc/android.hardware.faketouch.xml:system/etc/permissions/android.hardware.faketouch.xml \
	frameworks/native/data/etc/android.hardware.touchscreen.multitouch.jazzhand.xml:system/etc/permissions/android.hardware.touchscreen.multitouch.jazzhand.xml \
	frameworks/native/data/etc/android.software.sip.voip.xml:system/etc/permissions/android.software.sip.voip.xml \
	frameworks/native/data/etc/android.hardware.usb.host.xml:system/etc/permissions/android.hardware.usb.host.xml \
	frameworks/native/data/etc/android.hardware.usb.accessory.xml:system/etc/permissions/android.hardware.usb.accessory.xml

# for PDK build, include only when the dir exists
# too early to use $(TARGET_BUILD_PDK)
ifneq ($(wildcard packages/wallpapers/LivePicker),)
PRODUCT_COPY_FILES += \
	packages/wallpapers/LivePicker/android.software.live_wallpaper.xml:system/etc/permissions/android.software.live_wallpaper.xml
endif

PRODUCT_COPY_FILES += \
	device/fsl/hmi/rc.control:system/bin/control/rc.control \
	device/fsl/hmi/pm_install.sh:system/xbin/pm_install.sh \
	device/fsl/hmi/APK/ReliabilityApp.apk:system/hmi/ReliabilityApp.apk_

ifeq ($(WB_BUILD),true)
PRODUCT_COPY_FILES += \
	device/fsl/hmi/firmware/brcm/brcmfmac-sdio.bin:system/etc/firmware/brcm/brcmfmac-sdio.bin \
	device/fsl/hmi/firmware/brcm/brcmfmac-sdio.txt:system/etc/firmware/brcm/brcmfmac-sdio.txt \
	device/fsl/hmi/firmware/brcm/bcm4329.hcd:system/etc/firmware/brcm/BCM4329B1.hcd \
	device/fsl/hmi/rc.wifi:system/bin/wifi/rc.wifi
else
PRODUCT_COPY_FILES += \
	device/fsl/hmi/firmware/brcm/bcm4330_fw.bin:system/etc/firmware/brcm/brcmfmac-sdio.bin \
	device/fsl/hmi/firmware/brcm/bcm4330_nvram.txt:system/etc/firmware/brcm/brcmfmac-sdio.txt \
	device/fsl/hmi/firmware/brcm/bcm4330.hcd:system/etc/firmware/brcm/BCM4330B1.hcd \
	device/fsl/hmi/rc.wifi:system/bin/wifi/rc.wifi
endif

PRODUCT_COPY_FILES += \
	device/fsl/hmi/media_profiles_480p.xml:system/etc/media_profiles.xml
# Bluetooth support
PRODUCT_COPY_FILES += \
	system/bluetooth/data/main.nonsmartphone.conf:system/etc/bluetooth/main.conf

