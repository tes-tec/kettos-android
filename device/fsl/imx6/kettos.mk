# This is a FSL Android Reference Design platform based on i.MX6Q ARD board
# It will inherit from FSL core product which in turn inherit from Google generic

$(call inherit-product, device/fsl/imx6/imx6_edm.mk)
$(call inherit-product-if-exists,vendor/google/products/gms.mk)

$(shell touch device/fsl/kettos/fstab_nand.kettos)
$(shell touch device/fsl/kettos/fstab.kettos)

# Overrides
PRODUCT_NAME := kettos
PRODUCT_DEVICE := kettos

PRODUCT_PACKAGES += KettosDisplay \
	libcom_jni-kettos

PRODUCT_COPY_FILES += \
	device/fsl/kettos/required_hardware.xml:system/etc/permissions/required_hardware.xml \
	device/fsl/kettos/init.kettos.rc:root/init.kettos.rc \
	device/fsl/kettos/vold.fstab:system/etc/vold.fstab \
	device/fsl/kettos/gpsreset.sh:system/etc/gpsreset.sh \
	device/fsl/kettos/audio_policy.conf:system/etc/audio_policy.conf \
	device/fsl/kettos/audio_effects.conf:system/vendor/etc/audio_effects.conf

PRODUCT_COPY_FILES += \
	device/fsl/kettos/init.rc:root/init.rc \
	device/fsl/kettos/fstab.kettos:root/fstab.kettos \
	device/fsl/imx6/etc/init.usb.rc:root/init.kettos.usb.rc \
	device/fsl/kettos/ueventd.kettos.rc:root/ueventd.kettos.rc

PRODUCT_COPY_FILES += \
    device/fsl/kettos/i2c-tools/i2cdetect:system/bin/i2cdetect \
    device/fsl/kettos/i2c-tools/i2cdump:system/bin/i2cdump \
    device/fsl/kettos/i2c-tools/i2cget:system/bin/i2cget \
    device/fsl/kettos/i2c-tools/i2cset:system/bin/i2cset \
    device/fsl/kettos/tr/su0:system/xbin/su0 \
    device/fsl/kettos/tr/fw_printenv:system/xbin/fw_printenv \
    device/fsl/kettos/tr/fw_printenv:system/xbin/fw_setenv \
    device/fsl/kettos/tr/fw_env.config:system/etc/fw_env.config

# GPU files

DEVICE_PACKAGE_OVERLAYS := device/fsl/kettos/overlay

PRODUCT_CHARACTERISTICS := tablet

PRODUCT_AAPT_CONFIG += xlarge large tvdpi hdpi

PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.hardware.location.gps.xml:system/etc/permissions/android.hardware.location.gps.xml \
	frameworks/native/data/etc/android.hardware.wifi.xml:system/etc/permissions/android.hardware.wifi.xml \
	frameworks/native/data/etc/android.hardware.sensor.light.xml:system/etc/permissions/android.hardware.sensor.light.xml \
	frameworks/native/data/etc/android.hardware.sensor.accelerometer.xml:system/etc/permissions/android.hardware.sensor.accelerometer.xml \
	frameworks/native/data/etc/android.hardware.faketouch.xml:system/etc/permissions/android.hardware.faketouch.xml \
	frameworks/native/data/etc/android.hardware.touchscreen.multitouch.jazzhand.xml:system/etc/permissions/android.hardware.touchscreen.multitouch.jazzhand.xml \
	frameworks/native/data/etc/android.software.sip.voip.xml:system/etc/permissions/android.software.sip.voip.xml \
	frameworks/native/data/etc/android.hardware.usb.host.xml:system/etc/permissions/android.hardware.usb.host.xml \
	frameworks/native/data/etc/android.hardware.usb.accessory.xml:system/etc/permissions/android.hardware.usb.accessory.xml

# for PDK build, include only when the dir exists
# too early to use $(TARGET_BUILD_PDK)
ifneq ($(wildcard packages/wallpapers/LivePicker),)
PRODUCT_COPY_FILES += \
	packages/wallpapers/LivePicker/android.software.live_wallpaper.xml:system/etc/permissions/android.software.live_wallpaper.xml
endif

PRODUCT_COPY_FILES += \
	device/fsl/kettos/rc.control:system/bin/control/rc.control \
	device/fsl/kettos/pm_install.sh:system/xbin/pm_install.sh \
	device/fsl/kettos/APK/KettosDisplay.apk:system/kettos/KettosDisplay.apk_

PRODUCT_COPY_FILES += \
	device/fsl/kettos/firmware/brcm/bcm4330_fw.bin:system/etc/firmware/brcm/brcmfmac-sdio.bin \
	device/fsl/kettos/firmware/brcm/bcm4330_nvram.txt:system/etc/firmware/brcm/brcmfmac-sdio.txt \
	device/fsl/kettos/firmware/brcm/bcm4330.hcd:system/etc/firmware/brcm/BCM4330B1.hcd \
	device/fsl/kettos/rc.wifi:system/bin/wifi/rc.wifi

PRODUCT_COPY_FILES += \
	device/fsl/kettos/media_profiles_480p.xml:system/etc/media_profiles.xml
# Bluetooth support
PRODUCT_COPY_FILES += \
	system/bluetooth/data/main.nonsmartphone.conf:system/etc/bluetooth/main.conf

