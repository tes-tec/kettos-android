# This is a FSL Android Reference Design platform based on i.MX6Q ARD board
# It will inherit from FSL core product which in turn inherit from Google generic

$(call inherit-product, device/fsl/imx6/imx6.mk)
$(call inherit-product-if-exists,vendor/google/products/gms.mk)

# Overrides
PRODUCT_NAME := kettos_av
PRODUCT_DEVICE := kettos_av

PRODUCT_PACKAGES += SLATEDisplay \
	libcom_jni-kettos

PRODUCT_COPY_FILES += \
	device/fsl/kettos_av/required_hardware.xml:system/etc/permissions/required_hardware.xml \
	device/fsl/kettos_av/init.rc:root/init.freescale.rc \
	device/fsl/kettos_av/vold.fstab:system/etc/vold.fstab \
	device/fsl/common/input/prism.idc:system/usr/idc/prism.idc \
	device/fsl/kettos_av/audio_policy.conf:system/etc/audio_policy.conf \
	device/fsl/kettos_av/audio_effects.conf:system/vendor/etc/audio_effects.conf

PRODUCT_COPY_FILES += \
    device/fsl/kettos_av/i2c-tools/i2cdetect:system/bin/i2cdetect \
    device/fsl/kettos_av/i2c-tools/i2cdump:system/bin/i2cdump \
    device/fsl/kettos_av/i2c-tools/i2cget:system/bin/i2cget \
    device/fsl/kettos_av/i2c-tools/i2cset:system/bin/i2cset \
    device/fsl/kettos_av/tr/su0:system/xbin/su0 \
    device/fsl/kettos_av/tr/fw_printenv:system/xbin/fw_printenv \
    device/fsl/kettos_av/tr/fw_printenv:system/xbin/fw_setenv \
    device/fsl/kettos_av/tr/fw_env.config:system/etc/fw_env.config

# GPU files

DEVICE_PACKAGE_OVERLAYS := device/fsl/kettos_av/overlay

PRODUCT_CHARACTERISTICS := tablet

PRODUCT_AAPT_CONFIG += xlarge large tvdpi hdpi

PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.hardware.location.gps.xml:system/etc/permissions/android.hardware.location.gps.xml \
	frameworks/native/data/etc/android.hardware.wifi.xml:system/etc/permissions/android.hardware.wifi.xml \
	frameworks/native/data/etc/android.hardware.wifi.direct.xml:system/etc/permissions/android.hardware.wifi.direct.xml \
	frameworks/native/data/etc/android.hardware.sensor.light.xml:system/etc/permissions/android.hardware.sensor.light.xml \
	frameworks/native/data/etc/android.hardware.sensor.accelerometer.xml:system/etc/permissions/android.hardware.sensor.accelerometer.xml \
	frameworks/native/data/etc/android.hardware.faketouch.xml:system/etc/permissions/android.hardware.faketouch.xml \
	frameworks/native/data/etc/android.hardware.touchscreen.multitouch.jazzhand.xml:system/etc/permissions/android.hardware.touchscreen.multitouch.jazzhand.xml \
	frameworks/native/data/etc/android.software.sip.voip.xml:system/etc/permissions/android.software.sip.voip.xml \
	frameworks/native/data/etc/android.hardware.usb.host.xml:system/etc/permissions/android.hardware.usb.host.xml \
	frameworks/native/data/etc/android.hardware.usb.accessory.xml:system/etc/permissions/android.hardware.usb.accessory.xml

# for PDK build, include only when the dir exists
# too early to use $(TARGET_BUILD_PDK)
ifneq ($(wildcard packages/wallpapers/LivePicker),)
PRODUCT_COPY_FILES += \
	packages/wallpapers/LivePicker/android.software.live_wallpaper.xml:system/etc/permissions/android.software.live_wallpaper.xml
endif

PRODUCT_COPY_FILES += \
	device/fsl/kettos_av/rc.control:system/bin/control/rc.control \
	device/fsl/kettos_av/pm_install.sh:system/xbin/pm_install.sh \
	device/fsl/hmi/APK/ReliabilityApp.apk:system/kettos/ReliabilityApp.apk_ \
	device/fsl/hmi/APK/TouchTest.apk:system/kettos/TouchTest.apk_
