#
# Product-specific compile-time definitions.
#

ifeq ($(WB_BUILD),true)
include device/fsl/imx6/soc/imx6wb.mk
include device/fsl/hmi/build_id-wb.mk
else
include device/fsl/imx6/soc/imx6hmi.mk
include device/fsl/hmi/build_id.mk
endif

include device/fsl/imx6/BoardConfigCommon.mk
include device/fsl-proprietary/gpu-viv/fsl-gpu.mk

# sabresd_6dq default target for EXT4
BUILD_TARGET_FS ?= ext4
ifeq ($(BUILD_TARGET_FS),ubifs)
# build ubifs for nand devices
TARGET_USERIMAGES_USE_UBIFS := true
TARGET_USERIMAGES_USE_EXT4 := false
PRODUCT_COPY_FILES +=   \
        device/fsl/hmi/fstab_nand.hmi:root/fstab.hmi
else
# build for ext4
TARGET_USERIMAGES_USE_EXT4 := true
TARGET_USERIMAGES_USE_UBIFS := false
PRODUCT_COPY_FILES +=   \
        device/fsl/hmi/fstab.hmi:root/fstab.hmi
ifneq ($(LDB_BUILD),true)
PRODUCT_COPY_FILES +=   \
        device/fsl/hmi/initlogo.rle:root/initlogo.rle
endif
endif # BUILD_TARGET_FS

TARGET_BOOTLOADER_BOARD_NAME := HMI
PRODUCT_MODEL := HMI

# Wifi
BOARD_WLAN_VENDOR 			 := WANDBOARD
# for atheros vendor
ifeq ($(BOARD_WLAN_VENDOR),ATHEROS)
BOARD_WLAN_DEVICE			 := ar6003
BOARD_HAS_ATH_WLAN 			 := true
WPA_SUPPLICANT_VERSION			 := VER_0_8_ATHEROS
WIFI_DRIVER_MODULE_PATH          	 := "/system/lib/modules/ath6kl_sdio.ko"
WIFI_DRIVER_MODULE_NAME          	 := "ath6kl_sdio"
WIFI_DRIVER_MODULE_ARG           	 := "suspend_mode=3 wow_mode=2 ar6k_clock=26000000 ath6kl_p2p=1"
WIFI_DRIVER_P2P_MODULE_ARG       	 := "suspend_mode=3 wow_mode=2 ar6k_clock=26000000 ath6kl_p2p=1 debug_mask=0x2413"
WIFI_SDIO_IF_DRIVER_MODULE_PATH  	 := "/system/lib/modules/cfg80211.ko"
WIFI_SDIO_IF_DRIVER_MODULE_NAME  	 := "cfg80211"
WIFI_SDIO_IF_DRIVER_MODULE_ARG   	 := ""
WIFI_COMPAT_MODULE_PATH			 := "/system/lib/modules/compat.ko"
WIFI_COMPAT_MODULE_NAME			 := "compat"
WIFI_COMPAT_MODULE_ARG			 := ""
endif
ifeq ($(BOARD_WLAN_VENDOR),WANDBOARD)
BOARD_WLAN_DEVICE                        := bcmdhd
WPA_SUPPLICANT_VERSION			 := VER_0_8_ATHEROS
WIFI_DRIVER_MODULE_PATH          	 := "/system/bin/wifi/brcmfmac.ko"
WIFI_DRIVER_MODULE_NAME          	 := "brcmfmac"
endif
#for intel vendor
ifeq ($(BOARD_WLAN_VENDOR),INTEL)
BOARD_HOSTAPD_PRIVATE_LIB		 ?= private_lib_driver_cmd
BOARD_WPA_SUPPLICANT_PRIVATE_LIB 	 ?= private_lib_driver_cmd
WPA_SUPPLICANT_VERSION			 := VER_0_8_X
HOSTAPD_VERSION				 := VER_0_8_X
BOARD_WPA_SUPPLICANT_PRIVATE_LIB         := private_lib_driver_cmd_intel
WIFI_DRIVER_MODULE_PATH          	 := "/system/lib/modules/iwlagn.ko"
WIFI_DRIVER_MODULE_NAME          	 := "iwlagn"
WIFI_DRIVER_MODULE_PATH			 ?= auto
endif
BOARD_WPA_SUPPLICANT_DRIVER      	 := NL80211
BOARD_HOSTAPD_DRIVER             	 := NL80211
WIFI_TEST_INTERFACE			 := "wlan0"

BOARD_MODEM_VENDOR := AMAZON

BOARD_HAVE_HARDWARE_GPS := true
USE_ATHR_GPS_HARDWARE := true
USE_QEMU_GPS_HARDWARE := false

#for accelerator sensor, need to define sensor type here
BOARD_HAS_SENSOR := true
SENSOR_MMA8451 := true

# for recovery service
TARGET_SELECT_KEY := 28

# we don't support sparse image.
TARGET_USERIMAGES_SPARSE_EXT_DISABLED := true

# uncomment below lins if use NAND
#TARGET_USERIMAGES_USE_UBIFS = true

TARGET_KERNEL_MODULES := \
   kernel_imx/drivers/net/wireless/brcm80211/brcmfmac/brcmfmac.ko:system/bin/wifi/brcmfmac.ko	\
   kernel_imx/drivers/net/wireless/brcm80211/brcmutil/brcmutil.ko:system/bin/wifi/brcmutil.ko

ifeq ($(TARGET_USERIMAGES_USE_UBIFS),true)
UBI_ROOT_INI := device/fsl/hmi/ubi/ubinize.ini
TARGET_MKUBIFS_ARGS := -m 4096 -e 516096 -c 4096 -x none
TARGET_UBIRAW_ARGS := -m 4096 -p 512KiB $(UBI_ROOT_INI)
endif

ifeq ($(TARGET_USERIMAGES_USE_UBIFS),true)
ifeq ($(TARGET_USERIMAGES_USE_EXT4),true)
$(error "TARGET_USERIMAGES_USE_UBIFS and TARGET_USERIMAGES_USE_EXT4 config open in same time, please only choose one target file system image")
endif
endif

#BOARD_KERNEL_CMDLINE := console=ttymxc0,115200 init=/init video=mxcfb0:dev=hdmi,1920x1080M@60,if=RGB24,bpp=32 video=mxcfb1:off video=mxcfb2:off fbmem=28M vmalloc=400M androidboot.console=ttymxc0 androidboot.hardware=freescale
#BOARD_KERNEL_CMDLINE := console=ttymxc0,115200 init=/init video=mxcfb0:dev=hdmi,1280x720M@60,if=RGB24,bpp=32 video=mxcfb1:off video=mxcfb2:off fbmem=14M vmalloc=400M androidboot.console=ttymxc0 androidboot.hardware=freescale
#ifeq ($(SOLO_BUILD),true)
#BOARD_KERNEL_CMDLINE := console=ttymxc0,115200 init=/init video=mxcfb0:dev=hdmi,800x480M@60,if=RGB24,bpp=32 video=mxcfb1:off video=mxcfb2:off fbmem=10M gpumem=64M vmalloc=400M androidboot.console=ttymxc0 androidboot.hardware=hmi
#BOARD_KERNEL_CMDLINE := console=ttymxc0,115200 init=/init video=mxcfb0:dev=lcd,SEIKO-WVGA,if=RGB666,bpp=32 video=mxcfb1:off video=mxcfb2:off fbmem=10M gpumem=64M vmalloc=400M androidboot.console=ttymxc0 androidboot.hardware=hmi
#BOARD_KERNEL_CMDLINE := console=ttymxc0,115200 init=/init video=mxcfb0:dev=ldb,LDB-WSVGA,if=RGB666,bpp=32 video=mxcfb1:off video=mxcfb2:off fbmem=10M gpumem=64M vmalloc=400M androidboot.console=ttymxc0 androidboot.hardware=hmi
#else
ifeq ($(LDB_BUILD),true)
BOARD_KERNEL_CMDLINE := console=ttymxc0,115200 init=/init video=mxcfb0:dev=ldb,LDB-WSVGA,if=RGB666,bpp=32 video=mxcfb1:off video=mxcfb2:off fbmem=10M vmalloc=400M androidboot.console=ttymxc0 androidboot.hardware=hmi
else
BOARD_KERNEL_CMDLINE := console=ttymxc0,115200 init=/init video=mxcfb0:dev=lcd,SEIKO-WVGA,if=RGB666,bpp=32 video=mxcfb1:off video=mxcfb2:off fbmem=10M vmalloc=400M androidboot.console=ttymxc0 androidboot.hardware=hmi
endif
#endif
#BOARD_KERNEL_CMDLINE := console=ttymxc0,115200 init=/init video=mxcfb0:dev=ldb,bpp=32 video=mxcfb1:off video=mxcfb2:off fbmem=10M fb0base=0x27b00000 vmalloc=400M androidboot.console=ttymxc0 androidboot.hardware=freescale
#BOARD_KERNEL_CMDLINE := console=ttymxc0,115200 init=/init video=mxcfb0:dev=lcd,SEIKO-WVGA,if=RGB666,bpp=32 video=mxcfb1:dev=hdmi,1280x720M@60,if=RGB24,bpp=32 video=mxcfb2:off fbmem=8M,12M vmalloc=400M androidboot.console=ttymxc0 androidboot.hardware=freescale

ifeq ($(TARGET_USERIMAGES_USE_UBIFS),true)
#UBI boot command line.
# Note: this NAND partition table must align with MFGTool's config.
BOARD_KERNEL_CMDLINE +=  mtdparts=gpmi-nand:16m(bootloader),16m(bootimg),128m(recovery),-(root) gpmi_debug_init ubi.mtd=3
endif

BOARD_HAVE_BLUETOOTH := true
BOARD_HAVE_BLUETOOTH_BCM := true

# atheros 3k BT
#BOARD_USE_AR3K_BLUETOOTH := true
BOARD_BLUETOOTH_BDROID_BUILDCFG_INCLUDE_DIR := device/fsl/hmi/bluetooth

USE_ION_ALLOCATOR := false
USE_GPU_ALLOCATOR := true

# camera hal v2
IMX_CAMERA_HAL_V2 := true

# define frame buffer count
NUM_FRAMEBUFFER_SURFACE_BUFFERS := 3

ifeq ($(WB_BUILD),true)
TARGET_BOOTLOADER_CONFIG := 6solo:hmi_android_solo_config 6dl:hmi_android_dl_config
else
TARGET_BOOTLOADER_CONFIG := 6quad:edm_cf_imx6_android_quad_config 6dl:edm_cf_imx6_android_dl_config 6solo:edm_cf_imx6_android_solo_config
endif
