out/host/linux-x86/bin/mkbootimg \
    --kernel kernel_imx/arch/arm/boot/zImage \
    --ramdisk out/target/product/hmi/ramdisk.img \
    --cmdline "console=ttymxc0,115200 init=/init video=mxcfb0:dev=lcd,SEIKO-WVGA,if=RGB666,bpp=32 video=mxcfb1:off video=mxcfb2:off fbmem=10M vmalloc=400M androidboot.console=ttymxc0 androidboot.hardware=hmi" \
    --base 0x10800000 \
    --output boot.img
