#!/system/bin/sh

pm_install=`getprop persist.pm_install`
case "$pm_install" in
    1)  exit
    ;;
esac

pm uninstall com.touchrevolution.reliability
pm install /system/hmi/ReliabilityApp.apk_

setprop persist.pm_install 1
