package com.touchrev.update;

public class JNICommand {

    public static native boolean runCommand(String cmd);
    
    public static native String runCommand2(String cmd);
    
    static {
        System.loadLibrary("com_jni-update");
    }
}
