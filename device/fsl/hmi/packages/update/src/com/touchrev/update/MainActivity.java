
package com.touchrev.update;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

public class MainActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button reboot = (Button) findViewById(R.id.button_reboot);
        reboot.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                JNICommand.runCommand("reboot");
            }
        });

        CheckBox c = (CheckBox) findViewById(R.id.show_navi_bar);
        String navBar = JNICommand.runCommand2("getprop persist.use.show_navbar");
        boolean res = Boolean.valueOf(navBar.trim());
        c.setChecked(res);
        c.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
                JNICommand.runCommand("setprop persist.use.show_navbar " + arg1);
                JNICommand.runCommand("sync");
            }
        });

        TextView tv = (TextView) findViewById(R.id.textView1);
        String v_pd = JNICommand.runCommand2("getprop fusion.version_pd").trim();
        String v_lo = JNICommand.runCommand2("getprop fusion.version_lo").trim();
        String v_hi = JNICommand.runCommand2("getprop fusion.version_hi").trim();

        tv.setText(v_pd + ":" + v_lo + ":" + v_hi);

        Button b = (Button) findViewById(R.id.button1);
        b.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Thread t = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        JNICommand
                                .runCommand("busybox dd if=/mnt/extsd/u-boot-eng.tr.bin of=/dev/block/mmcblk0 bs=1k seek=1 skip=1");

                        JNICommand.runCommand("sync");

                        JNICommand.runCommand("mkdir -p /cache/recovery/");

                        String name = JNICommand.runCommand2("getprop ro.product.name");

                        JNICommand.runCommand("echo '--update_package=/sdcard/" + name.trim()
                                + "-ota-eng.tr.zip\n--show_text' > /cache/recovery/command");

                        JNICommand.runCommand("setprop persist.pm_install 0");

                        JNICommand.runCommand("sync");

                        JNICommand.runCommand("reboot recovery");
                    }
                });
                t.start();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
}
