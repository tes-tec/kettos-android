/*
 * Copyright 2009 Cedric Priscal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <termios.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <jni.h>
#include <stdio.h>
#include <android/log.h>
#include "com_touchrev_update_JNIComman.h"

#define LOG_TAG "JNICOMMAND"

#define LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)
#define LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define LOGW(...)  __android_log_print(ANDROID_LOG_WARN,LOG_TAG,__VA_ARGS__)
#define LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)
#define LOGF(...)  __android_log_print(ANDROID_LOG_FATAL,LOG_TAG,__VA_ARGS__)

jboolean JNICALL Java_com_touchrev_update_JNICommand_runCommand
  (JNIEnv *env, jclass thiz, jstring cmd)
{
	char su_cmd[1024];

	const char *cmd_utf = (*env)->GetStringUTFChars(env, cmd, NULL);
	sprintf(su_cmd, "su0 -c \"%s\"", cmd_utf);
	(*env)->ReleaseStringUTFChars(env, cmd, cmd_utf);

	LOGD("Running command : %s", su_cmd);
	int ret = system(su_cmd);
	LOGD("system() ret = %d", ret);

	return (ret == 0) ? JNI_TRUE : JNI_FALSE;
}

JNIEXPORT jstring JNICALL Java_com_touchrev_update_JNICommand_runCommand2(
		JNIEnv * env, jclass thiz, jstring cmd) {
	LOGD("=====runCommand2 in=====");
	char su_cmd[1024];

	const char *cmd_utf = (*env)->GetStringUTFChars(env, cmd, NULL);
	sprintf(su_cmd, "su0 -c \"%s\"", cmd_utf);
	(*env)->ReleaseStringUTFChars(env, cmd, cmd_utf);

	LOGD("Running command2 : %s", su_cmd);
	FILE *fpipe;
	char line[256];
	char result[1024];
	memset(result, 0, sizeof result);
	if (!(fpipe = (FILE*) popen(su_cmd, "r"))) {
		LOGD("=====runCommand2 out:popen=====");
		return NULL;
	}
	LOGD("after popen");
	while (fgets(line, sizeof line, fpipe)) {
		LOGD("get2 line: %s", line);
		strcat(result, line);
		memset(line, 0, sizeof line);
	}
	pclose(fpipe);
	LOGD("get2 result: %s", result);
	if (!IsUTF8(result, strlen(result))) {
		LOGD("=====runCommand2 out:utf8error=====");
		return NULL;
	}
	LOGD("=====runCommand2 out=====");
	return (*env)->NewStringUTF(env, result);
}

int IsUTF8(const void* pBuffer, long size) {
	int IsUTF8 = 1;
	unsigned char* start = (unsigned char*) pBuffer;
	unsigned char* end = (unsigned char*) pBuffer + size;
	while (start < end) {
		if (*start < 0x80) {
			start++;
		} else if (*start < (0xC0)) {
			IsUTF8 = 0;
			break;
		} else if (*start < (0xE0)) {
			if (start >= end - 1)
				break;
			if ((start[1] & (0xC0)) != 0x80) {
				IsUTF8 = 0;
				break;
			}
			start += 2;
		} else if (*start < (0xF0)) {
			if (start >= end - 2)
				break;
			if ((start[1] & (0xC0)) != 0x80 || (start[2] & (0xC0)) != 0x80) {
				IsUTF8 = 0;
				break;
			}
			start += 3;
		} else {
			IsUTF8 = 0;
			break;
		}
	}
	return IsUTF8;
}
