LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_PRELINK_MODULE    := false
LOCAL_SRC_FILES         := JNICommand.c
LOCAL_CFLAGS            += -Idalvik/libnativehelper/include/nativehelper
LOCAL_C_INCLUDES        += com_touchrev_update_JNIComman.h
LOCAL_SHARED_LIBRARIES  := liblog
LOCAL_MODULE            := libcom_jni-update
LOCAL_MODULE_TAGS       := optional

include $(BUILD_SHARED_LIBRARY)
