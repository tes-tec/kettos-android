
package com.touchrev.kettos.display;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import android.view.ActionMode;
import android.view.View;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.ViewGroup;

import com.touchrev.kettos.display.help.AndroidBug5497Workaround;
import com.touchrev.kettos.display.help.BrowserDBHelper;
import com.touchrev.kettos.display.help.BrowserDBHelper.BookMark;
import com.touchrev.kettos.display.help.JNICommand;

import org.xwalk.core.XWalkNavigationHistory;
import org.xwalk.core.XWalkPreferences;
import org.xwalk.core.XWalkResourceClient;
import org.xwalk.core.XWalkSettings;
import org.xwalk.core.XWalkUIClient;
import org.xwalk.core.XWalkView;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class Browser extends Activity {
    private final static String TAG = "Browser";
    public final static String URL = "URL";
    private DoubleTapWebView webView;
    private ProgressBar progressBar;
    private LinearLayout controlBar;
    private Button go;
    private Button home;
    private Button mark;
    private Button back;
    private AnimationSet asIn;
    private AnimationSet asOut;
    private EditText urlEditText;
    private GestureLibrary gestureLib;
    private LinearLayout bookMarkList;
    private LinearLayout bookMarkBlackBackground;
    private LinearLayout bookMarkWhiteBackground;
    private RelativeLayout browserRoot;
    private Button bookMarkDone;
    private BrowserDBHelper dbh;
    private SQLiteDatabase db;
    private ListView bookMarkListView;
    private BookMarkListViewAdapter bookMarkListViewAdapter;
    private LinearLayout saveHeader;
    private LinearLayout editHeader;
    private boolean bookMarkListEditable = false;
    private boolean bookMarkListButtonShow = false;
    private View main;
    private Button closeButton;
    private Button helpButton;
    private LinearLayout buttonsBar;

    private ActionMode mActionMode = null;

    @Override
    public void onActionModeStarted(ActionMode mode) {
        if (mActionMode == null) {
            mActionMode = mode;
            Menu menu = mode.getMenu();
            // Remove the default menu items (select all, copy, paste, search)
            menu.clear();
        }
        super.onActionModeStarted(mode);
    }

    @Override
    public void onActionModeFinished(ActionMode mode) {
        mActionMode = null;
        super.onActionModeFinished(mode);
    }

    private class MyResourceClient extends XWalkResourceClient {
        public MyResourceClient(XWalkView view) {
            super(view);
        }

        @Override
        public void onLoadFinished(XWalkView view, String url) {
            Log.d(TAG, "onLoadFinished:url = " + url);

            checkBack();
            if (urlEditText != null) {
                urlEditText.setText(url);
            }
            KettosDisplayApplication.browsingUrl = url;

            super.onLoadFinished(view, url);

            Log.d(TAG, "H/W acc = " + webView.isHardwareAccelerated());
            view.clearCache(true);
            // view.freeMemory();
        }

        @Override
        public void onLoadStarted(XWalkView view, String url) {
            // Log.d(TAG, "onLoadStarted:url = " + url);
            super.onLoadStarted(view, url);
        }

        @Override
        public void onProgressChanged(XWalkView view, int progressInPercent) {
            if (progressInPercent == 100) {
                progressBar.setVisibility(View.GONE);
            } else {
                if (progressBar.getVisibility() == View.GONE)
                    progressBar.setVisibility(View.VISIBLE);
                progressBar.setProgress(progressInPercent);
            }
            super.onProgressChanged(view, progressInPercent);
        }

        @Override
        public void onReceivedLoadError(XWalkView view, int errorCode, String description,
                String failingUrl) {
            Log.d(TAG, "onReceivedLoadError");
            super.onReceivedLoadError(view, errorCode, description, failingUrl);
        }

        @Override
        public WebResourceResponse shouldInterceptLoadRequest(XWalkView view, String url) {
            // Log.d(TAG, "shouldInterceptLoadRequest:url = " + url);
            return super.shouldInterceptLoadRequest(view, url);
        }
    }

    private class MyUIClient extends XWalkUIClient {
        public MyUIClient(XWalkView view) {
            super(view);
        }

        @Override
        public void onRequestFocus(XWalkView view) {
            Log.d(TAG, "onRequestFocus");
            super.onRequestFocus(view);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (webView != null) {
            webView.onNewIntent(intent);
        }

        String url = intent.getStringExtra(URL);
        if (url != null && url != "") {
            doGo(url);
        }

        super.onNewIntent(intent);
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_MENU) {
        }
        return true;
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause");
        // JNICommand.runCommand("setprop persist.use.navbar true");
        super.onPause();
        if (webView != null) {
            webView.pauseTimers();
            webView.onHide();
        }
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume");
        // main.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        // JNICommand.runCommand("setprop persist.use.navbar false");
        super.onResume();
        if (webView != null) {
            webView.resumeTimers();
            webView.onShow();
        }
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy");

        super.onDestroy();

        if (webView != null) {
            webView.onDestroy();
        }

        // review !!!
        // deleteDatabase("webview.db");
        // deleteDatabase("webviewCache.db");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (webView != null) {
            webView.onActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void setKeyboardVisibilityForUrl(boolean visible) {
        InputMethodManager imm = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);
        if (visible) {
            imm.showSoftInput(webView, InputMethodManager.SHOW_IMPLICIT);
        } else {
            imm.hideSoftInputFromWindow(webView.getWindowToken(), 0);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");

        // ANIMATABLE_XWALK_VIEW preference key MUST be set before XWalkView
        // creation.
        // XWalkPreferences.setValue(XWalkPreferences.ANIMATABLE_XWALK_VIEW,
        // true);

        JNICommand
                .runCommand("echo 0 > /sys/devices/system/cpu/cpufreq/interactive/go_hispeed_load");

        super.onCreate(savedInstanceState);
        main = View.inflate(this, R.layout.browser, null);
        // main.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        setContentView(main);

        // LinearLayout parent = (LinearLayout) findViewById(R.id.container);
        // LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
        // LinearLayout.LayoutParams.MATCH_PARENT,
        // LinearLayout.LayoutParams.MATCH_PARENT);
        // webView = new DoubleTapWebView(this, this);
        // parent.addView(webView, params);

        webView = (DoubleTapWebView) findViewById(R.id.webView);
        progressBar = (ProgressBar) findViewById(R.id.progressbar_browser);

        // webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        // turn on debugging
//        XWalkPreferences.setValue(XWalkPreferences.REMOTE_DEBUGGING, true);

        webView.setResourceClient(new MyResourceClient(webView));
        webView.setUIClient(new MyUIClient(webView));

        final GestureDetector dg = new GestureDetector(this,
                new GestureDetector.SimpleOnGestureListener() {
                    public boolean onDoubleTap(MotionEvent e) {
                        if (controlBar.getVisibility() == View.GONE) {
                            controlBarMoveIn();
                        } else {
                            controlBarMoveOut();
                        }
                        webView.doubleTap = true;
                        return false;
                    }
                    public void onLongPress(MotionEvent e) {
                        Log.d(TAG, "onLongPress");
                        return;
                    }
                });
        webView.setGestureDetector(dg);

        webView.setFocusableInTouchMode(true);
        webView.requestFocusFromTouch();
        // webView.requestFocus();

        webView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick");
                // setKeyboardVisibilityForUrl(false);
            }
        });

        webView.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Log.d(TAG, "onLongClick");
                return true;
            }
        });
//        webView.setLongClickable(false);

        XWalkSettings webSettings = webView.getSettings();

        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);

        // webSettings.setBuiltInZoomControls(true);
        // webSettings.setDisplayZoomControls(true);
        // webSettings.setSupportZoom(true);

        // webSettings.setLoadWithOverviewMode(true);
        // webSettings.setUseWideViewPort(true);

        // webSettings.setRenderPriority(RenderPriority.HIGH);
        // webSettings.setPluginState(android.webkit.WebSettings.PluginState.ON_DEMAND);

        webSettings.setAppCacheEnabled(false);
        // webSettings.setAppCacheMaxSize(1);
        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);

        dbh = KettosDisplayApplication.getBrowserDBHelper();
        db = dbh.getWritableDatabase();
        controlBar = (LinearLayout) findViewById(R.id.controlbar);
        urlEditText = (EditText) findViewById(R.id.browser_edittext_url);

        browserRoot = (RelativeLayout) findViewById(R.id.browser_root);

        gestureLib = GestureLibraries.fromRawResource(this, R.raw.gestures);
        bookMarkListViewAdapter = new BookMarkListViewAdapter(this);
        if (!gestureLib.load()) {
            Log.e(TAG, "load gesture fail");
        }

        urlEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    setKeyboardVisibilityForUrl(hasFocus);
                    urlEditText.setText(webView.getUrl());
                }
            }
        });

        webView.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // Log.d(TAG, "onFocusChange:hasFocus = " + hasFocus);
            }
        });

        asIn = (AnimationSet) AnimationUtils.loadAnimation(this, R.anim.browser_bar_in);
        asIn.setAnimationListener(new AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                controlBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }
        });
        asOut = (AnimationSet) AnimationUtils.loadAnimation(this, R.anim.browser_bar_out);
        asOut.setAnimationListener(new AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                controlBar.setVisibility(View.GONE);
            }
        });
        go = (Button) findViewById(R.id.browser_button_go);
        go.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                doGo(urlEditText.getText().toString().trim());
            }
        });
        home = (Button) findViewById(R.id.browser_button_home);
        home.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                doSaveHomePage();
                return true;
            }
        });
        home.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!KettosDisplayApplication.isStartUpDiscoverDevicesScreen()) {
                    String homePage = KettosDisplayApplication.getHomePage();
                    if (!"".equals(homePage))
                        doGo(homePage);
                } else {
                    Intent intent = new Intent();
                    intent.setClass(Browser.this, DisplayControl.class);
                    intent.putExtra(DisplayControl.PAGE_KEY, DisplayControl.DISCOVER_DEVICES);
                    startActivity(intent);
                    finish();
                }
            }
        });
        back = (Button) findViewById(R.id.browser_button_back);
        back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                doBack();
            }
        });
        mark = (Button) findViewById(R.id.browser_button_mark);
        mark.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showBookMarkList(true);
            }
        });
        Intent intent = getIntent();
        String url = intent.getStringExtra(URL);
        if (url != null && url != "") {
            doGo(url);
        }

        closeButton = (Button) findViewById(R.id.browser_button_close);
        helpButton = (Button) findViewById(R.id.browser_button_help);
        closeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // finish();
                controlBarMoveOut();
                showDisplayControl();
            }
        });
        helpButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showDeviceHelp();
            }
        });
        buttonsBar = (LinearLayout) findViewById(R.id.browser_bottom_bar);

        AndroidBug5497Workaround.assistActivity(this);
    }

    private void checkBack() {
        if (webView.getNavigationHistory().canGoBack()) {
            back.setClickable(true);
            back.setTextColor(getResources().getColor(R.color.title_text_color));
        } else {
            back.setClickable(false);
            back.setTextColor(getResources().getColor(R.color.hint_gray));
        }
    }

    private void showBookMarkList(boolean open) {
        bookMarkListButtonShow = false;
        if (open) {
            if (bookMarkList == null) {
                bookMarkList = (LinearLayout) View.inflate(this, R.layout.browser_bookmark_list,
                        null);
                bookMarkBlackBackground = (LinearLayout) bookMarkList
                        .findViewById(R.id.book_mark_black_background);
                bookMarkBlackBackground.setOnTouchListener(new OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (bookMarkListButtonShow == true) {
                            loadBookMarkList(bookMarkListEditable);
                        }
                        return true;
                    }
                });
                bookMarkWhiteBackground = (LinearLayout) bookMarkList
                        .findViewById(R.id.book_mark_white_background);
                bookMarkWhiteBackground.setOnTouchListener(new OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (bookMarkListButtonShow == true) {
                            loadBookMarkList(bookMarkListEditable);
                        }
                        return true;
                    }
                });

                bookMarkListView = (ListView) bookMarkList.findViewById(R.id.book_mark_listview);

                bookMarkDone = (Button) bookMarkList.findViewById(R.id.button_bookmark_done);
                bookMarkDone.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showBookMarkList(false);
                        setKeyboardVisibilityForUrl(false);
                    }
                });
                LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT,
                        LayoutParams.MATCH_PARENT);
                browserRoot.addView(bookMarkList, lp);
            }
            loadBookMarkList(true);
            bookMarkList.setVisibility(View.VISIBLE);
        } else {
            bookMarkList.setVisibility(View.GONE);
        }
    }

    private View createEditHeader() {
        if (editHeader == null) {
            editHeader = (LinearLayout) View.inflate(this, R.layout.book_mark_item_edit_button,
                    null);
            Button editButton = (Button) editHeader.findViewById(R.id.button_book_mark_item_edit);
            editButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    loadBookMarkList(true);
                }
            });
        }
        return editHeader;
    }

    private View createSaveHeader() {
        if (saveHeader == null) {
            saveHeader = (LinearLayout) View.inflate(this, R.layout.book_mark_item_edit, null);
            final EditText nameEdit = (EditText) saveHeader
                    .findViewById(R.id.book_mark_item_edit_name);
            final EditText urlEdit = (EditText) saveHeader
                    .findViewById(R.id.book_mark_item_edit_url);
            Button save = (Button) saveHeader.findViewById(R.id.button_book_mark_item_save);
            save.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    // add bookmark
                    String name = nameEdit.getText().toString().trim();
                    if ("".equals(name)) {
                        CharSequence hint = null;
                        if ((hint = nameEdit.getHint()) != null)
                            name = hint.toString();
                    }
                    String url = urlEdit.getText().toString().trim();
                    dbh.addBookMark(name, url, db);
                    // replace the layout
                    loadBookMarkList(false);
                }
            });
        }
        EditText nameEdit = (EditText) saveHeader.findViewById(R.id.book_mark_item_edit_name);
        EditText urlEdit = (EditText) saveHeader.findViewById(R.id.book_mark_item_edit_url);
        nameEdit.setText("");
        nameEdit.setHint(webView.getTitle());
        urlEdit.setText(webView.getUrl());

        return saveHeader;
    }

    private void loadBookMarkList(boolean edit) {
        bookMarkListButtonShow = false;
        createSaveHeader();
        createEditHeader();
        bookMarkListView.removeHeaderView(editHeader);
        bookMarkListView.removeHeaderView(saveHeader);
        bookMarkListView.setAdapter(null);
        bookMarkListView.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (bookMarkListButtonShow == true) {
                    loadBookMarkList(bookMarkListEditable);
                    return true;
                }
                return false;
            }
        });
        bookMarkListEditable = edit;
        if (edit) {
            bookMarkListView.addHeaderView(saveHeader);
            bookMarkListViewAdapter.refreshBookMark();
            bookMarkListView.setAdapter(bookMarkListViewAdapter);
        } else {
            bookMarkListView.addHeaderView(editHeader);
            bookMarkListViewAdapter.refreshBookMark();
            bookMarkListView.setAdapter(bookMarkListViewAdapter);
        }
    }

    private void showDisplayControl() {
        Intent intent = new Intent();
        // intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.setClass(this, DisplayControl.class);
        startActivity(intent);
        finish();
    }

    private void showDeviceHelp() {
        // Toast.makeText(this, "show devices help", Toast.LENGTH_LONG).show();
        if (!isHelpOpen)
            showHelp(true);
    }

    protected RelativeLayout helpView;
    protected Button closeHelpButton;
    protected TextView help;
    protected boolean isHelpOpen = false;

    private void showHelp(boolean open) {
        if (open) {
            if (helpView == null) {
                helpView = (RelativeLayout) View.inflate(this, R.layout.help, null);
            }
            browserRoot.addView(helpView);

            closeHelpButton = (Button) helpView.findViewById(R.id.button_help_close);
            closeHelpButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    showHelp(false);
                }
            });

            help = (TextView) helpView.findViewById(R.id.help_textview);
            help.setText(Html.fromHtml(getResources().getString(
                    R.string.double_tap_help_text)));

            isHelpOpen = true;
        } else {
            browserRoot.removeView(helpView);
            isHelpOpen = false;
        }
    }

    private void controlBarMoveIn() {
        if (controlBar.getVisibility() == View.GONE) {
            controlBar.startAnimation(asIn);
            urlEditText.setEnabled(true);
        }
        if (buttonsBar != null) {
            buttonsBar.setVisibility(View.VISIBLE);
        }
    }

    private void controlBarMoveOut() {
        if (controlBar.getVisibility() == View.VISIBLE) {
            controlBar.startAnimation(asOut);
            urlEditText.setEnabled(false);
        }
        if (buttonsBar != null) {
            buttonsBar.setVisibility(View.GONE);
        }
    }

    private void doBack() {
        if (webView.getNavigationHistory().canGoBack()) {
            webView.getNavigationHistory().navigate(
                    XWalkNavigationHistory.Direction.BACKWARD, 1);
        }
        checkBack();
    }

    private void doGo(String url) {
        URL r;
        try {
            r = new URL(url);
        } catch (MalformedURLException e1) {
            try {
                Log.d(TAG, "url is not URL " + url);
                r = new URL("http", url, "");
                url = r.toString();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        checkBack();
        webView.load(url, null);
        KettosDisplayApplication.browsingUrl = url;
        urlEditText.setText(url);
        Log.d(TAG, "url:" + url + " webView.getUrl:" + webView.getUrl());
    }

    private void doSaveHomePage() {
        KettosDisplayApplication.setHomePage(KettosDisplayApplication.browsingUrl);
        Toast.makeText(this, "Set " + KettosDisplayApplication.browsingUrl + " as the homepage",
                Toast.LENGTH_SHORT).show();
    }

    class BookMarkListViewAdapter extends BaseAdapter {
        private ArrayList<BookMark> bookMarks;
        private Context context;

        public BookMarkListViewAdapter(Context context) {
            bookMarks = dbh.getAllBookMark(db);
            this.context = context;
        }

        public void refreshBookMark() {
            bookMarks = dbh.getAllBookMark(db);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final BookMark bm = (BookMark) getItem(position);
            if (convertView == null) {
                // set delete
                convertView = (FlingLayout) View.inflate(context, R.layout.book_mark_item, null);
            }
            convertView.setClickable(true);

            final EditText editName = (EditText) convertView
                    .findViewById(R.id.book_mark_item_edittext_name);
            final EditText editUrl = (EditText) convertView
                    .findViewById(R.id.book_mark_item_edittext_url);
            final TextView name = (TextView) convertView
                    .findViewById(R.id.book_mark_item_text_name);
            final TextView url = (TextView) convertView.findViewById(R.id.book_mark_item_text_url);
            final Button delete = (Button) convertView
                    .findViewById(R.id.button_book_mark_item_delete);
            final Button update = (Button) convertView
                    .findViewById(R.id.button_book_mark_item_update);

            final GestureDetector bookMarkItemGD = new GestureDetector(context,
                    new OnGestureListener() {
                        @Override
                        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                                float velocityY) {
                            if (!bookMarkListButtonShow) {
                                if (e2.getRawX() > e1.getRawX()) {
                                    delete.setVisibility(View.VISIBLE);
                                    update.setVisibility(View.INVISIBLE);
                                    editName.setVisibility(View.GONE);
                                    editUrl.setVisibility(View.GONE);
                                    name.setVisibility(View.VISIBLE);
                                    url.setVisibility(View.VISIBLE);
                                } else {
                                    update.setVisibility(View.VISIBLE);
                                    delete.setVisibility(View.INVISIBLE);
                                    editName.setVisibility(View.VISIBLE);
                                    editName.setHint(name.getText());
                                    editUrl.setVisibility(View.VISIBLE);
                                    editUrl.setHint(url.getText());
                                    name.setVisibility(View.GONE);
                                    url.setVisibility(View.GONE);
                                }
                                bookMarkListButtonShow = true;
                            }
                            return true;
                        }

                        @Override
                        public boolean onDown(MotionEvent e) {
                            return false;
                        }

                        @Override
                        public void onShowPress(MotionEvent e) {
                        }

                        @Override
                        public boolean onSingleTapUp(MotionEvent e) {
                            if (bookMarkListButtonShow == true) {
                                loadBookMarkList(bookMarkListEditable);
                                return true;
                            } else {
                                doGo(url.getText().toString());
                                showBookMarkList(false);
                                controlBarMoveOut();
                                return true;
                            }
                        }

                        @Override
                        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
                                float distanceY) {
                            return false;
                        }

                        @Override
                        public void onLongPress(MotionEvent e) {
                        }
                    });

            ((FlingLayout) convertView).setGestureDetector(bookMarkItemGD);

            delete.setTag("delete:" + bm.index);
            delete.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    // delete bookmark
                    String index = (String) v.getTag();
                    String[] res = index.split(":");
                    Log.d(TAG, "delete bookmark index:" + res[1]);
                    dbh.delBookMark(res[1], db);
                    loadBookMarkList(bookMarkListEditable);
                }
            });

            update.setTag("save:" + bm.index);
            update.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    // update bookmark
                    String index = (String) v.getTag();
                    String[] res = index.split(":");
                    Log.d(TAG, "update bookmark index:" + index + " res[1]" + res[1]);
                    FlingLayout fl = (FlingLayout) v.getParent();
                    EditText editName = (EditText) fl
                            .findViewById(R.id.book_mark_item_edittext_name);
                    EditText editUrl = (EditText) fl.findViewById(R.id.book_mark_item_edittext_url);
                    String newName = editName.getText().toString();
                    if ("".equals(newName)) {
                        newName = editName.getHint().toString();
                    }
                    String newUrl = editUrl.getText().toString();
                    if ("".equals(newUrl)) {
                        newUrl = editUrl.getHint().toString();
                    }
                    dbh.updateBookMark(res[1], newName, newUrl, db);
                    loadBookMarkList(bookMarkListEditable);
                }
            });

            name.setText(bm.name);
            url.setText(bm.url);
            convertView.setOnTouchListener(new OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return false;
                }
            });

            return convertView;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public Object getItem(int position) {
            return bookMarks.get(position);
        }

        @Override
        public int getCount() {
            return bookMarks.size();
        }
    }
}
