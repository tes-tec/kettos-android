
package com.touchrev.kettos.display.help;

import com.touchrev.kettos.display.KettosScanner.DeviceInfo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

import java.util.ArrayList;

public class ScannerDBHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Scanner.db";
    private static final String TAG = "ScannerDBHelper";

    public static abstract class DeviceInfoEntry implements BaseColumns {
        public static final String TABLE_NAME = "DeviceInfo";
        public static final String COLUMN_NAME_DISPLAY_NAME = "display_name";
        public static final String COLUMN_NAME_IP_ADDRESS = "ip_address";
        public static final String COLUMN_NAME_SEARCH_IP = "search_ip";
    }

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_ENTRIES = "CREATE TABLE "
            + ScannerDBHelper.DeviceInfoEntry.TABLE_NAME + " ( "
            + ScannerDBHelper.DeviceInfoEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + ScannerDBHelper.DeviceInfoEntry.COLUMN_NAME_DISPLAY_NAME + TEXT_TYPE + COMMA_SEP
            + ScannerDBHelper.DeviceInfoEntry.COLUMN_NAME_IP_ADDRESS + TEXT_TYPE + COMMA_SEP
            + ScannerDBHelper.DeviceInfoEntry.COLUMN_NAME_SEARCH_IP + TEXT_TYPE +" )";

    private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS "
            + DeviceInfoEntry.TABLE_NAME;

    public ScannerDBHelper(Context context, String name, CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public ScannerDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    // public boolean videoInDB(String name, SQLiteDatabase db) {
    // Cursor c = db.query(BookMarkEntry.TABLE_NAME, new String[] {
    // BookMarkEntry._ID
    // }, BookMarkEntry.COLUMN_NAME_BOOKMARK_NAME + "= ?", new String[] {
    // name
    // }, null, null, null);
    // int res = c.getCount();
    // c.close();
    // if (res > 0) {
    // Log.d(TAG, "video in db");
    // return true;
    // } else {
    // Log.d(TAG, "video not in db");
    // return false;
    // }
    // }

    public long updateDeviceInfo(String index, String name, String url, String searchIp,SQLiteDatabase db) {
        ContentValues cv = new ContentValues();
        cv.put(DeviceInfoEntry.COLUMN_NAME_DISPLAY_NAME, name);
        cv.put(DeviceInfoEntry.COLUMN_NAME_IP_ADDRESS, url);
        cv.put(DeviceInfoEntry.COLUMN_NAME_SEARCH_IP, searchIp);
        long res = db.update(DeviceInfoEntry.TABLE_NAME, cv, DeviceInfoEntry._ID + " = ?",
                new String[] {
                    index
                });
        Log.d(TAG, "update DeviceInfo:" + name + " url:" + url + " :" + res);
        return res;
    }

    public long addDeviceInfo(String displayName, String ipAddress, String searchIp, SQLiteDatabase db) {
        ContentValues cv = new ContentValues();
        cv.put(DeviceInfoEntry.COLUMN_NAME_DISPLAY_NAME, displayName);
        cv.put(DeviceInfoEntry.COLUMN_NAME_IP_ADDRESS, ipAddress);
        cv.put(DeviceInfoEntry.COLUMN_NAME_SEARCH_IP, searchIp);
        long res = db.insert(DeviceInfoEntry.TABLE_NAME, null, cv);
        Log.d(TAG, "insert displayName:" + displayName + " ipAddress:" + ipAddress +  " searchIp:" + searchIp + " :" + res);
        return res;
    }

    public void delDeviceInfo(String index, SQLiteDatabase db) {
        int res = db.delete(DeviceInfoEntry.TABLE_NAME, DeviceInfoEntry._ID + " = ?", new String[] {
            index
        });
        Log.d(TAG, "delDeviceInfo num:" + res);
    }

    public void clearDeviceInfo(SQLiteDatabase db) {
        int res = db.delete(DeviceInfoEntry.TABLE_NAME, null, null);
        Log.d(TAG, "clearDeviceInfo num:" + res);
    }

    public ArrayList<DeviceInfo> getAllDeviceInfo(SQLiteDatabase db) {
        Cursor c = null;
        ArrayList<DeviceInfo> deviceInfos = new ArrayList<DeviceInfo>();
        try {
            c = db.query(DeviceInfoEntry.TABLE_NAME, null, null, null, null, null, null);
            if (c.moveToFirst()) {
                do {
                    DeviceInfo bm = new DeviceInfo();
                    bm.index = c.getString(c.getColumnIndex(DeviceInfoEntry._ID));
                    bm.displayName = c.getString(c
                            .getColumnIndex(DeviceInfoEntry.COLUMN_NAME_DISPLAY_NAME));
                    bm.ipAddress = c.getString(c
                            .getColumnIndex(DeviceInfoEntry.COLUMN_NAME_IP_ADDRESS));
                    bm.searchIp = c.getString(c
                            .getColumnIndex(DeviceInfoEntry.COLUMN_NAME_SEARCH_IP));
                    deviceInfos.add(bm);
                } while (c.moveToNext());
                return deviceInfos;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return deviceInfos;
    }
}
