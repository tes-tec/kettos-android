
package com.touchrev.kettos.display;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class BaseActivity extends Activity {
    private final static String TAG = "BaseActivity";
    protected Button backButton;
    protected Button homeButton;
    protected Button helpButton;
    protected Button closeButton;
    protected Button nextButton;
    protected TextView help;
    protected ScrollView helpSV;
    protected RelativeLayout content;
    protected TextView title;
    protected RelativeLayout base_root;
    protected RelativeLayout helpView;
    protected boolean isHelpOpen = false;
    protected boolean notScanning = true;
    protected View main;

    private int helpText = R.string.display_control_main_page_help_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // JNICommand.runCommand("setprop persist.use.navbar false");
        main = View.inflate(this, R.layout.base_activity, null);
        // main.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        setContentView(main);
        backButton = (Button) findViewById(R.id.button_back);
        helpButton = (Button) findViewById(R.id.button_help);
        base_root = (RelativeLayout) findViewById(R.id.base_root);
        content = (RelativeLayout) findViewById(R.id.content);
        title = (TextView) findViewById(R.id.title);
        homeButton = (Button) findViewById(R.id.title_button_home);
        homeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(BaseActivity.this, Browser.class);
                intent.putExtra(Browser.URL, KettosDisplayApplication.getHomePage());
                startActivity(intent);
                finish();
            }
        });
        helpButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isHelpOpen)
                    showHelp(true);
            }
        });
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause");
        // JNICommand.runCommand("setprop persist.use.navbar true");
        super.onPause();
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume");
        // main.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        // JNICommand.runCommand("setprop persist.use.navbar false");
        super.onResume();
    }

    public void setHelpText(int ht) {
        helpText = ht;
    }

    private void showHelp(boolean open) {
        if (open) {
            if (helpView == null) {
                helpView = (RelativeLayout) View.inflate(this, R.layout.help, null);
            }
            base_root.addView(helpView);

            closeButton = (Button) helpView.findViewById(R.id.button_help_close);
            closeButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    showHelp(false);
                }
            });

            nextButton = (Button) helpView.findViewById(R.id.button_help_next);
            nextButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });

            help = (TextView) helpView.findViewById(R.id.help_textview);
            help.setText(Html.fromHtml(getResources().getString(helpText)));

            helpSV = (ScrollView) findViewById(R.id.help_scrollview);
            helpSV.scrollTo(0, 0);

            isHelpOpen = true;
        } else {
            base_root.removeView(helpView);
            isHelpOpen = false;
        }
    }
}
