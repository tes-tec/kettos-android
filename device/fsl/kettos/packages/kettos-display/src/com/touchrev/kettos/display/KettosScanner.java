
package com.touchrev.kettos.display;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.touchrev.kettos.display.KettosScanner.DeviceInfo;
import com.touchrev.kettos.display.help.Help;
import com.touchrev.kettos.display.help.ScannerDBHelper;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

interface ScanStatusListenner {
    void onPrepare(int total, int progress, int succ, String ip, ArrayList<DeviceInfo> dis);

    void onFail(int total, int progress, int succ, String ip, ArrayList<DeviceInfo> dis);

    void onSucc(int total, int progress, int succ, String ip, DeviceInfo di,
            ArrayList<DeviceInfo> dis);

    void onScanFinish(int total, int succ, ArrayList<DeviceInfo> dis);

    void onScanStop(int total, int progress, int succ, ArrayList<DeviceInfo> dis);
}

public class KettosScanner extends Service {
    private final static String TAG = "KettosScanner";
    public int rangeFrom = 0;
    public int rangeTo = 19;
    private ArrayList<DeviceInfo> deviceInfos;
    private ScanStatusListenner scanStatusListenner;
    private Thread doScan;
    private boolean stopScanner = false;
    private int succ = 0;
    private Handler handler;
    private int progress;
    private DeviceInfo di;
    private final IBinder mBinder = new KettosScannerBinder();
    private ScannerDBHelper dbh;
    private SQLiteDatabase db;
    private SlashScreenReceiver slashScreenReceiver;

    public class SlashScreenReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            stopScaning();
            Log.d(TAG, "SLASH_SCREEN_START_UP stopScaning");
        }

    }

    public static class DeviceInfo {
        public String index;
        public String displayName;
        public String ipAddress;
        public String searchIp;
    }

    public class KettosScannerBinder extends Binder {
        KettosScanner getService() {
            return KettosScanner.this;
        }
    }

    private String getHostAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en
                    .hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr
                        .hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        return inetAddress.getHostAddress().toString();
                    }
                }
            }
        } catch (Exception ex) {
            Log.e("IP Address", ex.toString());
        }
        return null;
    }

    // public void startScan(int from, int to) {
    // startScan(getHostAddress(), from, to);
    // }

    public ArrayList<DeviceInfo> getScanResult() {
        return deviceInfos;
    }

    public boolean startScan(String fromIp, String toIp, String netMask, Handler handler,
            ScanStatusListenner l) {
        Log.d(TAG, "from:" + fromIp + " to:" + toIp + " netMask:" + netMask);
        this.handler = handler;
        scanStatusListenner = l;
        int mask = Help.ipToInt(netMask);
        int fIp = Help.ipToInt(fromIp);
        int tIp = Help.ipToInt(toIp);
        final int fIp_f = fIp;
        Log.d(TAG, "1fip:" + fIp + " 1tip:" + tIp);
        fIp = fIp & (~mask);
        tIp = tIp & (~mask);
        if (fIp > tIp) {
            return false;
        }
        final int count = tIp - fIp + 1;
        Log.d(TAG, "fip:" + fIp + " tip:" + tIp + " count:" + count);
        if (count > 0) {
            stopScanner = false;
            deviceInfos.clear();
            doScan = new Thread() {

                @Override
                public void run() {
                    for (succ = 0, progress = 0; progress < count; progress++) {
                        String ip = Help.intToIp(fIp_f + progress);
                        Log.d(TAG, progress + ":" + ip);
                        if (stopScanner) {
                            Log.d(TAG, "scanstop,progress: " + progress);
                            KettosScanner.this.handler.post(new Runnable() {

                                @Override
                                public void run() {
                                    scanStatusListenner.onScanStop(count, progress, succ,
                                            deviceInfos);
                                    dbh.clearDeviceInfo(db);
                                    for (Iterator<DeviceInfo> iterator = deviceInfos.iterator(); iterator
                                            .hasNext();) {
                                        DeviceInfo di = (DeviceInfo) iterator.next();
                                        dbh.addDeviceInfo(di.displayName, di.ipAddress,
                                                di.searchIp, db);
                                    }
                                }
                            });
                            return;
                        }
                        scan(ip, count);
                    }

                    Log.d(TAG, "finish,succ:" + succ);
                    KettosScanner.this.handler.post(new Runnable() {

                        @Override
                        public void run() {
                            scanStatusListenner.onScanFinish(count, succ, deviceInfos);
                            dbh.clearDeviceInfo(db);
                            for (Iterator<DeviceInfo> iterator = deviceInfos.iterator(); iterator
                                    .hasNext();) {
                                DeviceInfo di = (DeviceInfo) iterator.next();
                                dbh.addDeviceInfo(di.displayName, di.ipAddress, di.searchIp, db);
                            }
                        }
                    });
                }
            };
            doScan.start();
            return true;
        } else {
            return false;
        }
    }

    // public void startScan(String hostIp, int from, int to) {
    // String[] seg = hostIp.split(".");
    // ArrayList<String> ips = new ArrayList<String>();
    // for (int i = from; i < to - from + 1; i++) {
    // String targetHost = seg[0] + seg[1] + seg[2] + seg[3] + from;
    // String url = "http://" + targetHost + "/notindex.html";
    // Log.d(TAG, "ips add url:" + url);
    // ips.add(url);
    // }
    // startScan(ips);
    // }

    private void scan(final String ip, final int count) {
        Log.d(TAG, "prepare,progeress:" + progress + "; ip:" + ip);
        handler.post(new Runnable() {

            @Override
            public void run() {
                scanStatusListenner.onPrepare(count, progress, succ, ip, deviceInfos);
            }
        });
        String data = getDate(ip);
        if (data != null) {
            di = getDeviceInfo(data, ip);
        } else {
            di = null;
        }
        if (di != null) {
            succ++;
            Log.d(TAG, "succ,progress:" + progress + "; ip:" + ip);
            handler.post(new Runnable() {

                @Override
                public void run() {
                    scanStatusListenner.onSucc(count, progress, succ, ip, di, deviceInfos);
                }
            });
        } else {
            Log.d(TAG, "fail,progress:" + progress + "; ip" + ip);
            handler.post(new Runnable() {

                @Override
                public void run() {
                    scanStatusListenner.onFail(count, progress, succ, ip, deviceInfos);
                }
            });
        }
    }

    // public void startScan(final ArrayList<String> ips) {
    // final int count = ips.size();
    // stopSanner = false;
    // deviceInfos.clear();
    // doScan = new Thread() {
    //
    // @Override
    // public void run() {
    // succ = 0;
    // for (progress = 0; progress < count; progress++) {
    // scan(ips.get(progress), count);
    // }
    // Log.d(TAG, "finish,succ:" + succ);
    // handler.post(new Runnable() {
    //
    // @Override
    // public void run() {
    // scanStatusListenner.onScanFinish(count, succ, deviceInfos);
    // }
    // });
    // }
    // };
    // doScan.start();
    // }

    public void stopScaning() {
        stopScanner = true;
    }

    public DeviceInfo getDeviceInfo(String rawData, String searchIp) {
        String regEx = "Content-type: application/json[\\s]*\\{[\\S\\s]+\\}";
        Pattern pat = Pattern.compile(regEx);
        Matcher mat = pat.matcher(rawData);
        boolean res = mat.find();
        if (res) {
            String legalData = mat.group(0);
            String head = "{";
            int start = legalData.indexOf(head);
            legalData = legalData.substring(start);
            DeviceInfo di = new DeviceInfo();
            try {
                JSONObject jsonObject = new JSONObject(legalData);
                di.displayName = jsonObject.get("display_name").toString();
                di.ipAddress = jsonObject.get("ip_address").toString();
                di.searchIp = searchIp;
                deviceInfos.add(di);
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
            return di;
        } else {
            return null;
        }
    }

    public String getDate(String url) {
        url = "http://" + url + "/notindex.html";
        Log.d(TAG, "getDate url:" + url);
        HttpParams httpParameters = new BasicHttpParams();
        HttpGet get = new HttpGet(url);
        int timeoutConnection = 1000;
        HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
        HttpConnectionParams.setSoTimeout(httpParameters, timeoutConnection);
        HttpClient client = new DefaultHttpClient(httpParameters);
        try {
            HttpResponse response = client.execute(get);
            return EntityUtils.toString(response.getEntity());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");
        dbh = KettosDisplayApplication.getScannerDBhelper();
        db = dbh.getWritableDatabase();
        deviceInfos = dbh.getAllDeviceInfo(db);
        slashScreenReceiver = new SlashScreenReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SlashScreen.SLASH_SCREEN_START_UP);
        registerReceiver(slashScreenReceiver, intentFilter);
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        stopScaning();
        unregisterReceiver(slashScreenReceiver);
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind");
        return mBinder;
    }

    @Override
    public void onRebind(Intent intent) {
        Log.d(TAG, "onRebind");
        super.onRebind(intent);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }
}
