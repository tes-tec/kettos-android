
package com.touchrev.kettos.display.help;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

import java.util.ArrayList;

public class BrowserDBHelper extends SQLiteOpenHelper {

    public static class BookMark {
        public String name;
        public String url;
        public String index;
    }

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Browser.db";
    private static final String TAG = "BrowserDBHelper";

    public static abstract class BookMarkEntry implements BaseColumns {
        public static final String TABLE_NAME = "bookmark";
        public static final String COLUMN_NAME_BOOKMARK_NAME = "bookmark_name";
        public static final String COLUMN_NAME_BOOKMARK_URL = "bookmark_url";
    }

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_ENTRIES = "CREATE TABLE "
            + BrowserDBHelper.BookMarkEntry.TABLE_NAME + " ( " + BrowserDBHelper.BookMarkEntry._ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + BrowserDBHelper.BookMarkEntry.COLUMN_NAME_BOOKMARK_NAME + TEXT_TYPE + COMMA_SEP
            + BrowserDBHelper.BookMarkEntry.COLUMN_NAME_BOOKMARK_URL + TEXT_TYPE + " )";

    private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS "
            + BookMarkEntry.TABLE_NAME;

    public BrowserDBHelper(Context context, String name, CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public BrowserDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    // public boolean videoInDB(String name, SQLiteDatabase db) {
    // Cursor c = db.query(BookMarkEntry.TABLE_NAME, new String[] {
    // BookMarkEntry._ID
    // }, BookMarkEntry.COLUMN_NAME_BOOKMARK_NAME + "= ?", new String[] {
    // name
    // }, null, null, null);
    // int res = c.getCount();
    // c.close();
    // if (res > 0) {
    // Log.d(TAG, "video in db");
    // return true;
    // } else {
    // Log.d(TAG, "video not in db");
    // return false;
    // }
    // }

    public long updateBookMark(String index, String name, String url, SQLiteDatabase db) {
        ContentValues cv = new ContentValues();
        cv.put(BookMarkEntry.COLUMN_NAME_BOOKMARK_NAME, name);
        cv.put(BookMarkEntry.COLUMN_NAME_BOOKMARK_URL, url);
        long res = db.update(BookMarkEntry.TABLE_NAME, cv, BookMarkEntry._ID + " = ?",
                new String[] {
                    index
                });
        Log.d(TAG, "update bookmark:" + name + " url:" + url + " :" + res);
        return res;
    }

    public long addBookMark(String name, String url, SQLiteDatabase db) {
        ContentValues cv = new ContentValues();
        cv.put(BookMarkEntry.COLUMN_NAME_BOOKMARK_NAME, name);
        cv.put(BookMarkEntry.COLUMN_NAME_BOOKMARK_URL, url);
        long res = db.insert(BookMarkEntry.TABLE_NAME, null, cv);
        Log.d(TAG, "insert bookmark:" + name + " url:" + url + " :" + res);
        return res;
    }

    public void delBookMark(String index, SQLiteDatabase db) {
        int res = db.delete(BookMarkEntry.TABLE_NAME, BookMarkEntry._ID + " = ?", new String[] {
            index
        });
        Log.d(TAG, "delBookMark num:" + res);
    }

    public ArrayList<BookMark> getAllBookMark(SQLiteDatabase db) {
        Cursor c = null;
        ArrayList<BookMark> bookMarks = new ArrayList<BrowserDBHelper.BookMark>();
        try {
            c = db.query(BookMarkEntry.TABLE_NAME, null, null, null, null, null, null);
            if (c.moveToFirst()) {
                do {
                    BookMark bm = new BookMark();
                    bm.index = c.getString(c.getColumnIndex(BookMarkEntry._ID));
                    bm.name = c
                            .getString(c.getColumnIndex(BookMarkEntry.COLUMN_NAME_BOOKMARK_NAME));
                    bm.url = c.getString(c.getColumnIndex(BookMarkEntry.COLUMN_NAME_BOOKMARK_URL));
                    bookMarks.add(bm);
                } while (c.moveToNext());
                return bookMarks;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return bookMarks;
    }
}
