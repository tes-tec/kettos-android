
package com.touchrev.kettos.display;

import com.touchrev.kettos.display.help.ConnectionListener;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo.State;
import android.net.ethernet.EthernetManager;
import android.os.Binder;
import android.os.IBinder;
import android.os.SystemProperties;
import android.provider.Settings;
import android.provider.Settings.Secure;
import android.provider.Settings.System;
import android.util.Log;

import java.util.ArrayList;

class EthernetInfo {
    public String ip;
    public String mask;
    public String route;
    public String dns;
}

interface GetInfoCallBack {
    void onGetInfoDone(EthernetInfo ei);
}

interface SetDHCPCallBack {
    void onSetDhcpDone(boolean res);
}

interface SetStaticCallBack {
    void onSetStaticDone(boolean res);
}

public class DisplayEthernetManager extends Service {
    public static final String TAG = "DisplayEthernetManager";
    public static final String ETHERNET_CONN_MODE_DHCP = "dhcp";
    public static final String ETHERNET_CONN_MODE_MANUAL = "manual";
    public static final String ETHERNET_MODE = "ethernet_mode";
    public static boolean isCarrierIn;
    private String ip;
    private String mask;
    private String dns;
    private String route;
    private ArrayList<ConnectionListener> connectionListenerList;
    private final IBinder mBinder = new DisplayEthernetManagerBinder();
    private ConnectionChangeReceiver connectionChangeReceiver;
    private final static String nullIpInfo = "";
    private EthernetManager mEthManager;

    public class DisplayEthernetManagerBinder extends Binder {
        DisplayEthernetManager getService() {
            return DisplayEthernetManager.this;
        }
    }

    public void registPlugIn(ConnectionListener monitor) {
        connectionListenerList.add(monitor);
    }

    public void unRegistPlugIn(ConnectionListener monitor) {
        connectionListenerList.remove(monitor);
    }

    public void initConnect() {
    }

    public boolean checkNetwork() {
        // Log.d(TAG, "checkNetwork,isCarrierIn:" + isCarrierIn);
        boolean res = false;

        ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        State state = connManager.getNetworkInfo(ConnectivityManager.TYPE_ETHERNET).getState();
        if (State.CONNECTED == state) {
            res = true;
        }
        state = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState();
        if (State.CONNECTED == state) {
            res = true;
        }

        if (1 != mEthManager.getEthernetCarrierState()) {
            res = false;
        }
        Log.d(TAG, "checkNetwork,res:" + res);
        return res;
    }

    private void saveIpSettingsInfo(String ip, String mask, String router, String dns,
            boolean useStaticIp) {
        ContentResolver contentResolver = getContentResolver();

        if (ip != null)
            System.putString(contentResolver, System.ETHERNET_STATIC_IP, ip);
        if (router != null)
            System.putString(contentResolver, System.ETHERNET_STATIC_GATEWAY, router);
        if (mask != null)
            System.putString(contentResolver, System.ETHERNET_STATIC_NETMASK, mask);
        if (dns != null)
            System.putString(contentResolver, System.ETHERNET_STATIC_DNS1, dns);

        System.putString(contentResolver, System.ETHERNET_STATIC_DNS2, null);
        System.putInt(contentResolver, System.ETHERNET_USE_STATIC_IP, useStaticIp ? 1 : 0);

        boolean enable = Secure.getInt(getContentResolver(), Secure.ETHERNET_ON, 1) == 1;
        Log.d(TAG, "notify Secure.ETHERNET_ON changed. enable = " + enable);
        if (enable) {
            Log.d(TAG, "first disable");
            Secure.putInt(getContentResolver(), Secure.ETHERNET_ON, 0);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
            }
            Log.d(TAG, "second enable");
            Secure.putInt(getContentResolver(), Secure.ETHERNET_ON, 1);
        }
    }

    public void setStatic(String ip, String mask, String router, String dns,
            SetStaticCallBack callBack) {
        saveIpSettingsInfo(ip, mask, router, dns, true);
        KettosDisplayApplication.setStaticIP(ip);
        KettosDisplayApplication.setStaticMask(mask);
        KettosDisplayApplication.setStaticRouter(router);
        KettosDisplayApplication.setStaticDNS(dns);
        if (callBack != null)
            callBack.onSetStaticDone(true);
    }

    public void setDHCP(final SetDHCPCallBack callBack) {
        saveIpSettingsInfo(null, null, null, null, false);
        if (callBack != null)
            callBack.onSetDhcpDone(true);
    }

    public EthernetInfo getInfo(boolean isUseStaticIP) {
        if (isUseStaticIP) {
            getEthInfoFromStaticIP();
        } else {
            getEthInfoFromDhcp();
        }
        EthernetInfo ei = new EthernetInfo();
        ei.ip = this.ip;
        ei.mask = this.mask;
        ei.route = this.route;
        ei.dns = this.dns;
        return ei;
    }

    public EthernetInfo getInfo() {
        return getInfo(isUseStaticIP());
    }

    public boolean isUseStaticIP() {
        return Settings.System.getInt(getContentResolver(), Settings.System.ETHERNET_USE_STATIC_IP,
                1) == 1 ? true : false;
    }

    public class ConnectionChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e(TAG, "connection change");

            boolean success = false;

            for (ConnectionListener connectionListener : connectionListenerList) {
                connectionListener.onEtherNetChange(success);
            }
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");
        if (isUseStaticIP()) {
            EthernetInfo ei = getInfo();
            if ((ei.ip == null) || (ei.ip.equals(""))) {
                setStatic("192.168.92.5", "255.255.255.0", "192.168.92.1", "", null);
            }
        }
        connectionListenerList = new ArrayList<ConnectionListener>();
        connectionChangeReceiver = new ConnectionChangeReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(connectionChangeReceiver, filter);

        mEthManager = (EthernetManager) getSystemService(Context.ETHERNET_SERVICE);
        if (mEthManager == null) {
            Log.e(TAG, "get ethernet manager failed");
        }
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(connectionChangeReceiver);
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }

    @Override
    public void onRebind(Intent intent) {
        Log.d(TAG, "onBind");
        super.onRebind(intent);
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind");
        return mBinder;
    }

    public void getEthInfoFromDhcp() {
        String tempIpInfo;
        String iface = mEthManager.getEthernetIfaceName();

        tempIpInfo = SystemProperties.get("dhcp." + iface + ".ipaddress");
        if ((tempIpInfo != null) && (!tempIpInfo.equals(""))) {
            ip = tempIpInfo;
        } else {
            ip = nullIpInfo;
        }

        tempIpInfo = SystemProperties.get("dhcp." + iface + ".mask");
        if ((tempIpInfo != null) && (!tempIpInfo.equals(""))) {
            mask = tempIpInfo;
        } else {
            mask = nullIpInfo;
        }

        tempIpInfo = SystemProperties.get("dhcp." + iface + ".gateway");
        if ((tempIpInfo != null) && (!tempIpInfo.equals(""))) {
            route = tempIpInfo;
        } else {
            route = nullIpInfo;
        }

        tempIpInfo = SystemProperties.get("dhcp." + iface + ".dns1");
        if ((tempIpInfo != null) && (!tempIpInfo.equals(""))) {
            dns = tempIpInfo;
        } else {
            dns = nullIpInfo;
        }
    }

    public void getEthInfoFromStaticIP() {
        ip = Settings.System.getString(getContentResolver(), Settings.System.ETHERNET_STATIC_IP);
        mask = Settings.System.getString(getContentResolver(),
                Settings.System.ETHERNET_STATIC_NETMASK);
        route = Settings.System.getString(getContentResolver(),
                Settings.System.ETHERNET_STATIC_GATEWAY);
        dns = Settings.System.getString(getContentResolver(), Settings.System.ETHERNET_STATIC_DNS1);
    }
}
