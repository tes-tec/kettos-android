
package com.touchrev.kettos.display;

import static android.provider.Settings.System.SCREEN_OFF_TIMEOUT;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.IPowerManager;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.provider.Settings;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.touchrev.kettos.display.KettosScanner.DeviceInfo;
import com.touchrev.kettos.display.help.Help;
import com.touchrev.kettos.display.help.JNICommand;

import org.apache.http.conn.util.InetAddressUtils;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

public class DisplayControl extends BaseActivity implements ScanStatusListenner {

    private final static String TAG = "DisplayControl";
    public final static String PAGE_KEY = "PAGE_KEY";
    public final static int MAIN = 0;
    public final static int NETWORK_SETTING = 1;
    public final static int DISCOVER_DEVICES = 2;
    public final static int GENERIC_VIEW = 3;
    public final static int MAINTENANCE = 4;
    private boolean isShowDDSForStarUp = false;
    private int page;
    private boolean mIsDisplayEthernetBound;
    private boolean mIsKettosScannerBound;
    private Button networkSettingsButton;
    private Button discoverDevicesButton;
    private Button genericViewButton;
    private Button maintenanceButton;
    private Button displayResetButton;
    // private EthernetManager mEthManager;
    // private EthernetDevInfo mEthInfo;
    // display control
    private View displayControl;
    private SeekBar brightness;
    private SeekBar speakVolume;
    // network settings
    private boolean fastTime = true;
    private View networkSettings;
    private RadioButton useDHCP;
    private RadioButton useStatic;
    private Button ping;
    private Button DHCPRefresh;
    private EditText staticIP;
    private EditText staticMask;
    private EditText staticRouter;
    private EditText staticDNS;
    private EditText editPing;
    private TextView dhcpIP;
    private TextView dhcpMask;
    private TextView dhcpRouter;
    private TextView dhcpDNS;
    private TextView dhcpDuration;
    private Handler handler = new Handler();
    private boolean pingFinish;
    private boolean canback;
    // generic view
    private View genericView;
    private EditText genericEditUrl;
    private Button useGeneric;
    private Button useText;
    private Button useHelp;
    private Button genericGo;
    private CheckBox genericSetHome;
    // discover devices
    private View discoverDevices;
    private ListView devicesListView;
    private DevicesAdapt devicesAdapt;
    private Button refresh;
    private ProgressBar progressBar;
    private TextView progressInfo;
    private CheckBox setHomePage;
    private RadioButton selectedDevice;
    private RadioButton startupPage;
    private DisplayEthernetManager dm;
    private KettosScanner kettosScanner;
    private Dialog refreshDialog;
    private EditText from;
    private EditText to;
    private EditText mask;
    // Maintenance
    private View maintenance;
    private String kettosPackage;
    private Ringtone mRingtone;
    private TimerTask timerTask;
    private Timer timer;

    private void doOnClickDevice(View v) {
        TextView serverUrl = (TextView) v.findViewById(R.id.kettos_server_ip);
        if ((!serverUrl.getText().toString().equals(KettosDisplayApplication.getHomePage()))
                && (setHomePage != null && setHomePage.isChecked())
                && (selectedDevice != null && selectedDevice.isChecked())) {
            KettosDisplayApplication.setHomePage(serverUrl.getText().toString());
            Toast.makeText(DisplayControl.this, "set home page:" + serverUrl.getText().toString(),
                    Toast.LENGTH_LONG).show();
        } else {
            String url;
            if (serverUrl != null) {
                url = serverUrl.getText().toString();
                Intent intent = new Intent();
                // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setClass(DisplayControl.this, Browser.class);
                intent.putExtra(Browser.URL, url);
                startActivity(intent);
                finish();
                // DisplayControl.this.finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_MENU) {
            // super.openOptionsMenu();
        }
        return true;
    }

    private ServiceConnection mEthernetConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            dm = ((DisplayEthernetManager.DisplayEthernetManagerBinder) service).getService();
            Log.d(TAG, "dm connected");
        }

        public void onServiceDisconnected(ComponentName className) {
            dm = null;
            Log.d(TAG, "dm disconnected");
        }

    };

    private ServiceConnection mKettosConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            kettosScanner = ((KettosScanner.KettosScannerBinder) service).getService();
            devicesAdapt.setDeviceList(kettosScanner.getScanResult());
            devicesAdapt.notifyDataSetChanged();
            if (devicesAdapt.getCount() > 0) {
                TextView t = (TextView) findViewById(R.id.textView_refresh_info);
                t.setVisibility(View.VISIBLE);
            }
            Log.d(TAG, "KettosScanner connected");
        }

        public void onServiceDisconnected(ComponentName className) {
            kettosScanner.stopScaning();
            kettosScanner = null;
            Log.d(TAG, "KettosScanner disconnected");
        }

    };

    private void doBindDisplayEthernetManagerService() {
        Log.d(TAG, "doBindDisplayEthernetManagerService");
        bindService(new Intent(this, DisplayEthernetManager.class), mEthernetConnection,
                Context.BIND_AUTO_CREATE);
        mIsDisplayEthernetBound = true;
    }

    private void doBindKettosScannerService() {
        Log.d(TAG, "doBindKettosScannerService");
        bindService(new Intent(this, KettosScanner.class), mKettosConnection,
                Context.BIND_AUTO_CREATE);
        mIsKettosScannerBound = true;
    }

    private void doUnbindDisplayEthernetManagerService() {
        Log.d(TAG, "doUnbindDisplayEthernetManagerService:" + mIsDisplayEthernetBound);
        if (mIsDisplayEthernetBound) {
            unbindService(mEthernetConnection);
            mIsDisplayEthernetBound = false;
        }
    }

    private void doUnbindKettosScannerService() {
        Log.d(TAG, "doUnbindKettosScannerService:" + mIsKettosScannerBound);
        if (mIsKettosScannerBound) {
            unbindService(mKettosConnection);
            mIsKettosScannerBound = false;
        }
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop");
        super.onStop();
    }

    @Override
    protected void onRestart() {
        Log.d(TAG, "onRestart");
        super.onRestart();
    }

    @Override
    protected void onStart() {
        Log.d(TAG, "onStart");
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        doUnbindDisplayEthernetManagerService();
        doUnbindKettosScannerService();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Log.d(TAG, "onNewIntent");
        page = intent.getIntExtra(PAGE_KEY, MAIN);
        if (page == DISCOVER_DEVICES) {
            isShowDDSForStarUp = true;
        }
        showPage(page);
        super.onNewIntent(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        Intent intent = getIntent();
        page = intent.getIntExtra(PAGE_KEY, MAIN);
        canback = intent.getBooleanExtra(SlashScreen.CANBACK, false);
        stopAutoBrightness();
        doBindDisplayEthernetManagerService();
        if (page == DISCOVER_DEVICES) {
            isShowDDSForStarUp = true;
        }
        showPage(page);
        backButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!notScanning) {
                    return;
                }
                if (page != MAIN) {
                    if (page == NETWORK_SETTING) {
                        saveSetting(useDHCP != null && useDHCP.isChecked());
                    } else {
                        showPage(MAIN);
                    }
                } else {
                    // finish when start this activity from browser
                    if (!isShowDDSForStarUp)
                        finish();
                }
            }
        });
    }

    private void setStatic() {
        String ip = staticIP.getText().toString().trim();
        String mask = staticMask.getText().toString().trim();
        String dns = staticDNS.getText().toString().trim();
        String router = staticRouter.getText().toString().trim();
        if ("".equals(ip)) {
            Toast.makeText(DisplayControl.this, "please input the static ip", Toast.LENGTH_SHORT)
                    .show();
            return;
        } else if (!InetAddressUtils.isIPv4Address(ip)) {
            Toast.makeText(DisplayControl.this, "ip is not legal", Toast.LENGTH_SHORT).show();
            return;
        } else if ("".equals(mask)) {
            Toast.makeText(DisplayControl.this, "please input the static subnet mask",
                    Toast.LENGTH_SHORT).show();
            return;
        } else if (!InetAddressUtils.isIPv4Address(mask)) {
            Toast.makeText(DisplayControl.this, "net mask is not legal", Toast.LENGTH_SHORT).show();
            return;
        } else if ("".equals(router)) {
            Toast.makeText(DisplayControl.this, "please input the static router",
                    Toast.LENGTH_SHORT).show();
            return;
        } else if (!InetAddressUtils.isIPv4Address(router)) {
            Toast.makeText(DisplayControl.this, "router is not legal", Toast.LENGTH_SHORT).show();
            return;
        } else if ("".equals(dns)) {
        } else if (!InetAddressUtils.isIPv4Address(dns)) {
            Toast.makeText(DisplayControl.this, "dns is not legal", Toast.LENGTH_SHORT).show();
            return;
        }

        // final ProgressDialog progress = ProgressDialog
        // .show(this, "", "Saving the network settings");
        // progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        Log.d(TAG, "dm.setStatic");

        dm.setStatic(ip, mask, router, dns, new SetStaticCallBack() {

            @Override
            public void onSetStaticDone(final boolean res) {
                handler.post(new Runnable() {

                    @Override
                    public void run() {
                        Log.d(TAG, "onSetStaticDone");
                        // progress.dismiss();
                        if (res) {
                            Toast.makeText(DisplayControl.this, "StaticIP successfully",
                                    Toast.LENGTH_LONG).show();
                            showPage(MAIN);
                        } else {
                            Toast.makeText(DisplayControl.this, "StaticIP fail", Toast.LENGTH_LONG)
                                    .show();
                        }
                    }
                });

            }
        });
    }

    private void saveSetting(boolean isDhcp) {
        Log.e(TAG, "saveSetting:" + isDhcp);
        try {
            if (isDhcp) {
                // final ProgressDialog progress = ProgressDialog.show(this, "",
                // "Saving the network settings");
                // progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);

                dm.setDHCP(new SetDHCPCallBack() {

                    @Override
                    public void onSetDhcpDone(final boolean res) {
                        handler.post(new Runnable() {

                            @Override
                            public void run() {
                                // progress.dismiss();
                                if (res) {
                                    Toast.makeText(DisplayControl.this, "DHCP successfully",
                                            Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(DisplayControl.this, "DHCP fail",
                                            Toast.LENGTH_LONG).show();
                                }
                            }
                        });
                    }

                });
                showPage(MAIN);
            } else {
                setStatic();
            }
        } catch (Exception e) {
            Log.e(TAG, "save static setting error");
            e.printStackTrace();
        }
    }

    private View addView(View v) {
        content.removeAllViews();
        LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        content.addView(v, lp);
        return v;
    }

    private void refreshDHCPInfo() {
        EthernetInfo ei = dm.getInfo(false);
        dhcpIP.setText(ei.ip);
        dhcpMask.setText(ei.mask);
        dhcpDNS.setText(ei.dns);
        dhcpRouter.setText(ei.route);
    }

    private void staticInfor() {
        EthernetInfo ei = dm.getInfo(true);
        if (!"".equals(ei.ip)) {
            Log.e(TAG, "get static ip:" + ei.ip);
            staticIP.setText(ei.ip);
        }
        if (!"".equals(ei.mask)) {
            Log.e(TAG, "get static mask:" + ei.mask);
            staticMask.setText(ei.mask);
        }
        if (!"".equals(ei.route)) {
            Log.e(TAG, "get static route:" + ei.route);
            staticRouter.setText(ei.route);
        }
        if (!"".equals(ei.dns)) {
            Log.e(TAG, "get static dns:" + ei.dns);
            staticDNS.setText(ei.dns);
        }
        // staticIP.setText(KettosDisplayApplication.getStaticIP());
        // staticMask.setText(KettosDisplayApplication.getStaticMask());
        // staticRouter.setText(KettosDisplayApplication.getStaticRouter());
        // staticDNS.setText(KettosDisplayApplication.getStaticDNS());
    }

    private void clearDHCPInfo() {
        dhcpIP.setText("");
        dhcpMask.setText("");
        dhcpRouter.setText("");
        dhcpDNS.setText("");
    }

    private void showNetworkSettings() {
        title.setText(R.string.text_network_settings);
        if (networkSettings == null) {
            networkSettings = View.inflate(this, R.layout.network_settings, null);
        }
        View v = addView(networkSettings);
        useDHCP = (RadioButton) v.findViewById(R.id.radio_dhcp);
        useDHCP.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    useStatic.setChecked(false);
                    DHCPRefresh.setClickable(true);
                    // DHCPRefresh.setTextColor(getResources()
                    // .getColor(R.color.radio_button_clickable));
                    refreshDHCPInfo();
                }
            }
        });
        useStatic = (RadioButton) v.findViewById(R.id.radio_static);
        useStatic.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    useDHCP.setChecked(false);
                    DHCPRefresh.setClickable(false);
                    // DHCPRefresh.setTextColor(getResources().getColor(
                    // R.color.radio_button_unclickable));
                    staticInfor();
                    clearDHCPInfo();
                }
            }
        });

        DHCPRefresh = (Button) v.findViewById(R.id.button_dhcp_refresh);
        DHCPRefresh.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                refreshDHCPInfo();
            }
        });
        staticIP = (EditText) v.findViewById(R.id.network_setting_static_ip);
        staticMask = (EditText) v.findViewById(R.id.network_setting_static_mask);
        staticRouter = (EditText) v.findViewById(R.id.network_setting_static_router);
        staticDNS = (EditText) v.findViewById(R.id.network_setting_static_dns);
        dhcpIP = (TextView) v.findViewById(R.id.network_setting_dhcp_ip);
        dhcpMask = (TextView) v.findViewById(R.id.network_setting_dhcp_mask);
        dhcpRouter = (TextView) v.findViewById(R.id.network_setting_dhcp_router);
        dhcpDNS = (TextView) v.findViewById(R.id.network_setting_dhcp_dns);
        editPing = (EditText) v.findViewById(R.id.network_settings_edit_ping);
        ping = (Button) v.findViewById(R.id.button_ping);
        ping.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog dialog = new ProgressDialog(DisplayControl.this);
                pingFinish = false;
                dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                dialog.setMessage("ping...");
                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        // main.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
                    }
                });
                dialog.show();

                final String host = editPing.getText().toString().trim();
                final int timeOut = 7000;
                Thread t = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        boolean res;
                        try {
                            if ("".equals(host)) {
                                res = false;
                            } else {
                                res = InetAddress.getByName(host).isReachable(timeOut);
                            }
                        } catch (UnknownHostException e) {
                            res = false;
                            e.printStackTrace();
                        } catch (IOException e) {
                            res = false;
                            e.printStackTrace();
                        }
                        final boolean res_f = res;
                        pingFinish = true;
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                dialog.dismiss();
                                AlertDialog.Builder builder = new AlertDialog.Builder(
                                        DisplayControl.this);
                                builder.setTitle("Ping");
                                if (res_f) {
                                    builder.setMessage("Host is OK");
                                } else {
                                    builder.setMessage("Host is no response");
                                }
                                builder.setNeutralButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        }).setOnDismissListener(
                                        new DialogInterface.OnDismissListener() {
                                            @Override
                                            public void onDismiss(DialogInterface dialog) {
                                                // main.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
                                            }
                                        }).show();
                            }
                        });
                    }
                });
                t.start();
            }
        });
        if (!dm.isUseStaticIP()) {
            useDHCP.setChecked(true);
            useStatic.setChecked(false);
            DHCPRefresh.setClickable(true);
            if (!fastTime) {
                refreshDHCPInfo();
            }
            fastTime = false;
        } else {
            useDHCP.setChecked(false);
            useStatic.setChecked(true);
            DHCPRefresh.setClickable(false);
            staticInfor();
        }
    }

    private void showGenericView() {
        title.setText(R.string.text_generic_view);
        if (genericView == null) {
            genericView = View.inflate(this, R.layout.generic_view, null);
        }
        View v = addView(genericView);
        genericEditUrl = (EditText) v.findViewById(R.id.generic_view_edit_url);
        genericEditUrl.setText(KettosDisplayApplication.browsingUrl);
        genericSetHome = (CheckBox) v.findViewById(R.id.checkBox_set_homepage);
        genericSetHome.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    String url = genericEditUrl.getText().toString();
                    if (url != null && !"".equals(url)) {
                        KettosDisplayApplication.setHomePage(url);
                    }
                }
            }
        });
        genericGo = (Button) v.findViewById(R.id.button_go);
        genericGo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(DisplayControl.this, Browser.class);
                String url = genericEditUrl.getText().toString();
                if (url != null && !("".equals(url))) {
                    intent.putExtra(Browser.URL, url);
                    startActivity(intent);
                    finish();
                }
            }
        });
        final URL url;
        try {
            url = new URL(KettosDisplayApplication.browsingUrl);
            final String host = url.getHost();
            if (host != null && !("".equals(host))) {
                useGeneric = (Button) v.findViewById(R.id.button_use_generic);
                useGeneric.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        genericEditUrl.setText(url.getProtocol() + "://" + host + "/generic");
                    }
                });
                useText = (Button) v.findViewById(R.id.button_use_text);
                useText.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        genericEditUrl.setText(url.getProtocol() + "://" + host + "/text");
                    }
                });
                useHelp = (Button) v.findViewById(R.id.button_use_help);
                useHelp.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        genericEditUrl.setText(url.getProtocol() + "://" + host + "/help");
                    }
                });
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private boolean checkIp(String fromIp, String toIp) {
        int fIp = Help.ipToInt(fromIp);
        int tIp = Help.ipToInt(toIp);
        Log.d(TAG, "checkIp fip:" + fIp + " tip:" + tIp);
        if (fIp > tIp) {
            return false;
        } else {
            return true;
        }
    }

    private void showDevicesRefreshDialog() {
        EthernetInfo ei = dm.getInfo();
        if (refreshDialog == null) {
            refreshDialog = new Dialog(this, R.style.DevicesRefreshDialog);
            refreshDialog.setContentView(R.layout.devices_refresh_dialog);
            if (from == null)
                from = (EditText) refreshDialog.findViewById(R.id.edittext_from);
            if (to == null)
                to = (EditText) refreshDialog.findViewById(R.id.edittext_to);
            if (mask == null)
                mask = (EditText) refreshDialog.findViewById(R.id.edittext_netmask);
            if (ei.ip != null && (!"".equals(ei.ip))) {
                try {
                    int ip = Help.ipToInt(ei.ip);
                    int Imask = Help.ipToInt("255.255.255.0");
                    String Sfrom = Help.intToIp((ip & Imask) + 1);
                    String Sto = Help.intToIp((ip & Imask) + 20);
                    KettosDisplayApplication.setKettosScannerFrom(Sfrom);
                    KettosDisplayApplication.setKettosScannerTo(Sto);
                    KettosDisplayApplication.setKettosScannerNetMask("255.255.255.0");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        if (KettosDisplayApplication.getKettosScannerFrom() != null
                && KettosDisplayApplication.getKettosScannerTo() != null
                && KettosDisplayApplication.getKettosScannerTo() != null) {
            from.setText(KettosDisplayApplication.getKettosScannerFrom());
            to.setText(KettosDisplayApplication.getKettosScannerTo());
            mask.setText(KettosDisplayApplication.getKettosScannerNetMask());
        } else {
            if (ei.ip != null && (!"".equals(ei.ip))) {
                try {
                    int ip = Help.ipToInt(ei.ip);
                    int Imask = Help.ipToInt("255.255.255.0");
                    String Sfrom = Help.intToIp((ip & Imask) + 1);
                    String Sto = Help.intToIp((ip & Imask) + 255);
                    KettosDisplayApplication.setKettosScannerFrom(Sfrom);
                    KettosDisplayApplication.setKettosScannerTo(Sto);
                    KettosDisplayApplication.setKettosScannerNetMask("255.255.255.0");
                    from.setText(KettosDisplayApplication.getKettosScannerFrom());
                    to.setText(KettosDisplayApplication.getKettosScannerTo());
                    mask.setText(KettosDisplayApplication.getKettosScannerNetMask());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        Button cancel = (Button) refreshDialog.findViewById(R.id.dialog_button_cancel);
        Button search = (Button) refreshDialog.findViewById(R.id.dialog_button_search);
        search.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (!InetAddressUtils.isIPv4Address(from.getText().toString())) {
                        Toast.makeText(DisplayControl.this, "from is not ipv4", Toast.LENGTH_LONG)
                                .show();
                        return;
                    } else if (!InetAddressUtils.isIPv4Address(to.getText().toString())) {
                        Toast.makeText(DisplayControl.this, "to is not ipv4", Toast.LENGTH_LONG)
                                .show();
                        return;
                    } else if (!InetAddressUtils.isIPv4Address(mask.getText().toString())) {
                        Toast.makeText(DisplayControl.this, "please input correct subnetmask",
                                Toast.LENGTH_LONG).show();
                        return;
                    } else if (!checkIp(from.getText().toString(), to.getText().toString())) {
                        Toast.makeText(DisplayControl.this, "to should bigger than from",
                                Toast.LENGTH_LONG).show();
                        return;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
                refreshDialog.dismiss();
                // main.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
                KettosDisplayApplication.setKettosScannerFrom(from.getText().toString());
                KettosDisplayApplication.setKettosScannerTo(to.getText().toString());
                KettosDisplayApplication.setKettosScannerNetMask(mask.getText().toString());
                if (!kettosScanner.startScan(from.getText().toString(), to.getText().toString(),
                        mask.getText().toString(), handler, DisplayControl.this)) {
                    refresh.setText(R.string.button_refresh);
                } else {
                    refresh.setText(R.string.button_cancel);
                    TextView t = (TextView) findViewById(R.id.textView_refresh_info);
                    t.setVisibility(View.INVISIBLE);
                }
            }
        });
        cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshDialog.dismiss();
                // main.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
                refresh.setText(R.string.button_refresh);
            }
        });
        refreshDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                // main.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
                return false;
            }
        });
        refreshDialog.show();
    }

    private void setDisabledTextViews(ViewGroup dp) {
        for (int x = 0, n = dp.getChildCount(); x < n; x++) {
            View v = dp.getChildAt(x);
            if (v instanceof TextView) {
                v.setEnabled(false);
            } else if (v instanceof ViewGroup) {
                setDisabledTextViews((ViewGroup) v);
            }
        }
    }

    private String checkUpdate() {
        String res = null;
        ArrayList<String> list = Help.getFileList(new File("/storage/extsd"));
        for (String fileName : list) {
            PackageManager pm = getPackageManager();
            PackageInfo info = pm.getPackageArchiveInfo(fileName, PackageManager.GET_ACTIVITIES);
            if (info != null) {
                Log.d(TAG, "update apk ver:" + info.applicationInfo.packageName + "-"
                        + info.versionCode);
                if (kettosPackage != null && kettosPackage.equals(info.applicationInfo.packageName))
                    res = fileName;
            }
        }
        return res;
    }

    private void showMaintenance() {
        title.setText(R.string.text_maintenance);
        if (maintenance == null) {
            maintenance = View.inflate(this, R.layout.maintenance, null);
        }
        View v = addView(maintenance);
        try {
            PackageManager pm = getPackageManager();
            PackageInfo info = pm.getPackageInfo(getPackageName(), PackageManager.GET_ACTIVITIES);
            kettosPackage = info.applicationInfo.packageName;
            Log.d(TAG, "kettos ver:" + info.versionCode);
            TextView ver = (TextView) maintenance.findViewById(R.id.textview_ver);
            ver.setText("SLATE Display version :" + info.versionName);
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
        Button update = (Button) maintenance.findViewById(R.id.button_update);
        update.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                final String fileName;
                if ((fileName = checkUpdate()) != null) {
                    Log.d(TAG, "update KettosDisplay");

                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    Log.d(TAG, "apk file name:" + fileName);
                    intent.setDataAndType(Uri.fromFile(new File(fileName)),
                            "application/vnd.android.package-archive");
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(DisplayControl.this, "No update in SDCard", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });

        Button displaySetting = (Button) maintenance.findViewById(R.id.button_display_setting);
        displaySetting.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // Intent intent = new Intent(Settings.ACTION_DISPLAY_SETTINGS);
                // startActivity(intent);

                AlertDialog.Builder builder = new AlertDialog.Builder(DisplayControl.this);
                int value = Settings.System.getInt(getContentResolver(), SCREEN_OFF_TIMEOUT, 15000);
                final String[] values = DisplayControl.this.getResources().getStringArray(
                        R.array.screen_timeout_values);
                ArrayList<String> list = new ArrayList<String>(Arrays.asList(values));
                int i = list.indexOf(Integer.valueOf(value).toString());
                builder.setTitle("Sleep")
                        .setSingleChoiceItems(R.array.screen_timeout_entries, i,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        try {
                                            int value = Integer.parseInt((String) values[which]);
                                            Settings.System.putInt(getContentResolver(),
                                                    SCREEN_OFF_TIMEOUT, value);
                                            dialog.dismiss();
                                        } catch (NumberFormatException e) {
                                            Log.e(TAG, "could not persist screen timeout setting",
                                                    e);
                                        }
                                    }
                                }).setOnKeyListener(new DialogInterface.OnKeyListener() {

                            @Override
                            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                                // main.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
                                return false;
                            }
                        }).setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                // main.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
                            }
                        }).setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();
            }
        });
/*
        Button appSetting = (Button) maintenance.findViewById(R.id.button_app_setting);
        appSetting.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // Intent intent = new
                // Intent(Settings.ACTION_APPLICATION_SETTINGS);
                // startActivity(intent);

                if (SystemProperties.get("persist.use.show_navbar").equals("false")) {
                    new AlertDialog.Builder(DisplayControl.this).setTitle("Android")
                            .setMessage("This will take a reboot!")
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @SuppressWarnings("deprecation")
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    // getPackageManager().clearPackagePreferredActivities(getPackageName());

                                    PackageManager pm = getPackageManager();
                                    IntentFilter filter = new IntentFilter();

                                    filter.addAction("android.intent.action.MAIN");
                                    filter.addCategory("android.intent.category.HOME");
                                    filter.addCategory("android.intent.category.DEFAULT");

                                    ComponentName component = new ComponentName(
                                            "com.android.launcher",
                                            "com.android.launcher2.Launcher");
                                    ComponentName[] components = new ComponentName[] {
                                            new ComponentName("com.touchrev.kettos.display",
                                                    ".SlashScreen"), component
                                    };

                                    pm.clearPackagePreferredActivities("com.touchrev.kettos.display");
                                    pm.addPreferredActivity(filter,
                                            IntentFilter.MATCH_CATEGORY_EMPTY,
                                            components, component);

                                    JNICommand.runCommand("setprop persist.use.show_navbar true");
                                    JNICommand.runCommand("sync");
                                    JNICommand.runCommand("reboot");
                                }
                            }).setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    // main.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
                                }
                            }).create().show();
                } else {
                    Intent intent = new Intent();
                    ComponentName component = new
                            ComponentName("com.android.launcher",
                                    "com.android.launcher2.Launcher");
                    intent.setComponent(component);
                    startActivity(intent);
                    finish();
                }
            }
        });
*/
    }

    private void showDiscoverDevices() {
        if (devicesAdapt == null) {
            devicesAdapt = new DevicesAdapt();
        }
        if (!mIsKettosScannerBound) {
            doBindKettosScannerService();
        } else {
            devicesAdapt.setDeviceList(kettosScanner.getScanResult());
            devicesAdapt.notifyDataSetChanged();
        }
        title.setText(R.string.text_discover_devices);
        if (discoverDevices == null) {
            discoverDevices = View.inflate(this, R.layout.discover_devices, null);
        }
        View v = addView(discoverDevices);
        devicesListView = (ListView) v.findViewById(R.id.listView_discover_device);
        devicesListView.setAdapter(devicesAdapt);
        if (devicesAdapt.getCount() > 0) {
            TextView t = (TextView) findViewById(R.id.textView_refresh_info);
            t.setVisibility(View.VISIBLE);
        }
        refresh = (Button) v.findViewById(R.id.button_refresh);
        progressBar = (ProgressBar) v.findViewById(R.id.refresh_progress);
        progressBar.setMax(100);
        progressInfo = (TextView) v.findViewById(R.id.textview_progress_info);
        refresh.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (refresh.getText().equals(getText(R.string.button_refresh))) {
                    // refresh.setText(R.string.button_cancel);
                    showDevicesRefreshDialog();
                } else {
                    refresh.setText(R.string.button_refresh);
                    kettosScanner.stopScaning();
                }
            }
        });
        startupPage = (RadioButton) v.findViewById(R.id.radio_startup_page);
        startupPage.setClickable(false);
        startupPage.setTextColor(getResources().getColor(R.color.radio_button_unclickable));
        startupPage.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                KettosDisplayApplication.setStartUpDiscoverDevicesScreen(isChecked);
            }
        });

        selectedDevice = (RadioButton) v.findViewById(R.id.radio_selected_device);
        selectedDevice.setTextColor(getResources().getColor(R.color.radio_button_unclickable));
        selectedDevice.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    if (KettosDisplayApplication.getHomePage().equals("")
                            || KettosDisplayApplication.getHomePage()
                                    .equals("http://[about:blank]")) {
                        KettosDisplayApplication
                                .setDefaultHomePage("http://192.168.92.10/slate/index.html");
                    }
                    KettosDisplayApplication.notStartUpDiscoverDevicesScreen();
                }
            }
        });

        setHomePage = (CheckBox) v.findViewById(R.id.checkBox_set_homepage);
        if (setHomePage.isChecked()) {
            selectedDevice.setClickable(true);
            startupPage.setClickable(true);
            startupPage.setTextColor(getResources()
                    .getColor(R.color.radio_button_clickable));
            selectedDevice.setTextColor(getResources().getColor(
                    R.color.radio_button_clickable));
        } else {
            selectedDevice.setClickable(false);
            startupPage.setClickable(false);
            startupPage.setTextColor(getResources().getColor(
                    R.color.radio_button_unclickable));
            selectedDevice.setTextColor(getResources().getColor(
                    R.color.radio_button_unclickable));
        }
        setHomePage.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    selectedDevice.setClickable(true);
                    startupPage.setClickable(true);
                    startupPage.setTextColor(getResources()
                            .getColor(R.color.radio_button_clickable));
                    selectedDevice.setTextColor(getResources().getColor(
                            R.color.radio_button_clickable));
                } else {
                    selectedDevice.setClickable(false);
                    startupPage.setClickable(false);
                    startupPage.setTextColor(getResources().getColor(
                            R.color.radio_button_unclickable));
                    selectedDevice.setTextColor(getResources().getColor(
                            R.color.radio_button_unclickable));
                }
            }
        });
    }

    private int getScreenBrightness() {
        int nowBrightnessValue = 0;
        ContentResolver resolver = getContentResolver();
        try {
            nowBrightnessValue = android.provider.Settings.System.getInt(resolver,
                    Settings.System.SCREEN_BRIGHTNESS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return nowBrightnessValue;
    }

    private void stopAutoBrightness() {
        Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE,
                Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
    }

    private void setBrightness(int brightness) {
        try {
            IPowerManager power = IPowerManager.Stub
                    .asInterface(ServiceManager.getService("power"));
            if (power != null) {
                // power.setBacklightBrightness(brightness);
                power.setTemporaryScreenBrightnessSettingOverride(brightness);
            }
            final ContentResolver resolver = DisplayControl.this.getContentResolver();
            Settings.System.putInt(resolver, Settings.System.SCREEN_BRIGHTNESS, brightness);
        } catch (RemoteException doe) {

        }
    }

    private void setScreenBrightness(ContentResolver resolver, int brightness) {
        Uri uri = android.provider.Settings.System.getUriFor(Settings.System.SCREEN_BRIGHTNESS);
        android.provider.Settings.System.putInt(resolver, Settings.System.SCREEN_BRIGHTNESS,
                brightness);
        resolver.notifyChange(uri, null);
    }

    private void setSpeakVolume(AudioManager audioManager, int volume) {
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0);
    }

    private void showMain() {
        title.setText(R.string.text_display_control);
        if (!canback) {
            backButton.setVisibility(View.INVISIBLE);
        } else {
            backButton.setVisibility(View.VISIBLE);
        }
        final AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        if (displayControl == null) {
            displayControl = View.inflate(this, R.layout.display_control, null);
            brightness = (SeekBar) displayControl.findViewById(R.id.seekBar_brightness);
            // brightness.setThumb(getResources().getDrawable(R.drawable.seekbar_thumb_background));
            brightness.setMax(225);
            int b = getScreenBrightness() - 30;
            if ((b) < 0) {
                brightness.setProgress(0);
            } else {
                brightness.setProgress(b);
            }
            brightness.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                }

                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    if (fromUser) {
                        setBrightness(30 + progress);
                    }
                }
            });

            if (mRingtone == null)
                mRingtone = RingtoneManager.getRingtone(DisplayControl.this,
                        Settings.System.DEFAULT_ALARM_ALERT_URI);

            if (mRingtone != null) {
                mRingtone.setStreamType(AudioManager.STREAM_MUSIC);
            }

            speakVolume = (SeekBar) displayControl.findViewById(R.id.seekBar_speak_volume);
            // speakVolume.setThumb(getResources().getDrawable(R.drawable.seekbar_thumb_background));
            speakVolume.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
            speakVolume.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
            speakVolume.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    if (timer != null) {
                        timer.cancel();
                    }
                    if (mRingtone != null) {
                        mRingtone.play();
                        timerTask = new TimerTask() {
                            @Override
                            public void run() {
                                mRingtone.stop();
                            }
                        };
                        timer = new Timer();
                        if (timer != null) {
                            timer.schedule(timerTask, 1500);
                        }
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                    if (timer != null) {
                        timer.cancel();
                    }
                }

                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    if (fromUser)
                        setSpeakVolume(audioManager, progress);
                    if (mRingtone != null) {
                        mRingtone.stop();
                    }
                }
            });
            networkSettingsButton = (Button) displayControl
                    .findViewById(R.id.button_network_settings);
            networkSettingsButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    showPage(NETWORK_SETTING);
                }
            });
            discoverDevicesButton = (Button) displayControl
                    .findViewById(R.id.button_discover_devices);
            discoverDevicesButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    showPage(DISCOVER_DEVICES);
                }
            });
            genericViewButton = (Button) displayControl.findViewById(R.id.button_generic_view);
            genericViewButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    showPage(GENERIC_VIEW);
                }
            });
            maintenanceButton = (Button) displayControl.findViewById(R.id.button_maintenance);
            maintenanceButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    showPage(MAINTENANCE);
                }
            });
            displayResetButton = (Button) displayControl.findViewById(R.id.button_display_reset);
            displayResetButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    new AlertDialog.Builder(DisplayControl.this).setTitle("Reboot")
                            .setMessage("Are you sure?")
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    JNICommand.runCommand("reboot");
                                }
                            }).setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    // main.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
                                }
                            }).create().show();
                }
            });
        }
        addView(displayControl);
    }

    private void showPage(int page) {
        this.page = page;
        backButton.setVisibility(View.VISIBLE);
        switch (page) {
            case MAIN:
                setHelpText(R.string.display_control_main_page_help_text);
                showMain();
                break;
            case DISCOVER_DEVICES:
                setHelpText(R.string.discover_devices_page_help_text);
                showDiscoverDevices();
                break;
            case NETWORK_SETTING:
                setHelpText(R.string.network_settings_page_help_text);
                showNetworkSettings();
                break;
            case GENERIC_VIEW:
                setHelpText(R.string.generic_view_page_help_text);
                showGenericView();
                break;
            case MAINTENANCE:
                setHelpText(R.string.display_settings_page_help_text);
                showMaintenance();
                break;
            default:
                break;
        }
    }

    @Override
    public void onPrepare(int total, int progress, int succ, String ip, ArrayList<DeviceInfo> dis) {
        int p = (int) ((float) progress / total * 100);
        notScanning = false;
        progressBar.setProgress(p);
        progressInfo.setText("checking; progress:" + progress + " ip:" + ip);
        Log.d(TAG, "onPrepare:" + p + ";" + ip + ";");
    }

    @Override
    public void onFail(int total, int progress, int succ, String ip, ArrayList<DeviceInfo> dis) {
        int p = (int) ((float) progress / total * 100);
        progressBar.setProgress(p);
        progressInfo.setText("fail:" + ip);
        Log.d(TAG, "onFail:" + p + ";" + ip);
    }

    @Override
    public void onSucc(int total, int progress, int succ, String ip, DeviceInfo di,
            ArrayList<DeviceInfo> dis) {
        int p = (int) ((float) progress / total * 100);
        TextView t = (TextView) findViewById(R.id.textView_refresh_info);
        t.setVisibility(View.VISIBLE);
        progressBar.setProgress(p);
        progressInfo.setText("success:" + ip);
        devicesAdapt.setDeviceList(dis);
        devicesAdapt.notifyDataSetChanged();
        Log.d(TAG, "onSucc:" + p + ";" + ip);
    }

    @Override
    public void onScanFinish(int total, int succ, ArrayList<DeviceInfo> dis) {
        notScanning = true;
        progressBar.setProgress(100);
        progressInfo.setText("finish; total:" + total + " success:" + succ);
        devicesAdapt.setDeviceList(dis);
        devicesAdapt.notifyDataSetChanged();
        refresh.setText(R.string.button_refresh);
        Log.d(TAG, "onScanFinish:" + total + ";" + succ);
    }

    @Override
    public void onScanStop(int total, int progress, int succ, ArrayList<DeviceInfo> dis) {
        int p = (int) ((float) progress / total * 100);
        notScanning = true;
        progressBar.setProgress(p);
        progressInfo.setText("cancel; total:" + total + " progress:" + progress + " success:"
                + succ);
        devicesAdapt.setDeviceList(dis);
        devicesAdapt.notifyDataSetChanged();
        refresh.setText(R.string.button_refresh);
        Log.d(TAG, "onScanStop:" + total + ";" + progress + ";" + succ);
    }

    class DevicesAdapt extends BaseAdapter {
        private ArrayList<KettosScanner.DeviceInfo> devicesArrayList;

        public DevicesAdapt() {
        }

        public void setDeviceList(ArrayList<KettosScanner.DeviceInfo> list) {
            devicesArrayList = list;
        }

        @Override
        public int getCount() {
            if (devicesArrayList != null) {
                return devicesArrayList.size();
            } else {
                return 0;
            }
        }

        @Override
        public Object getItem(int position) {
            return devicesArrayList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(DisplayControl.this, R.layout.discover_device_list_item,
                        null);
            }
            TextView serverUrl = (TextView) convertView.findViewById(R.id.kettos_server_ip);
            TextView serverName = (TextView) convertView.findViewById(R.id.kettos_server_name);
            try {
                KettosScanner.DeviceInfo device = (KettosScanner.DeviceInfo) devicesAdapt
                        .getItem(position);
                serverUrl.setText(device.searchIp);
                serverName.setText(device.displayName);
                convertView.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        doOnClickDevice(v);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
            return convertView;
        }
    }
}
