package com.touchrev.kettos.display.help;

public interface ConnectionListener {
    void onEtherNetChange(boolean plugIn);
}
