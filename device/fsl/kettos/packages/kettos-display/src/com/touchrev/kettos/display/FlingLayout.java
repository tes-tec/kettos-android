
package com.touchrev.kettos.display;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.LinearLayout;

public class FlingLayout extends LinearLayout {
    private final static String TAG = "FlingLayout";
    private GestureDetector gd;

    public void setGestureDetector(GestureDetector gd) {
        this.gd = gd;
    }

    public FlingLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FlingLayout(Context context) {
        super(context);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        gd.onTouchEvent(event);
        return super.onTouchEvent(event);
    }
}
