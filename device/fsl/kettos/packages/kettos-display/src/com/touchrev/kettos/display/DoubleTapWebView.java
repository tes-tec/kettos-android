
package com.touchrev.kettos.display;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;

import org.xwalk.core.XWalkView;

public class DoubleTapWebView extends XWalkView {
    private final static String TAG = "DoubleTapWebView";
    private GestureDetector gd;
    public boolean doubleTap = false;

    public void setGestureDetector(GestureDetector gd) {
        this.gd = gd;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        // Log.d(TAG, "onInterceptTouchEvent");
        gd.onTouchEvent(ev);

        // disable double tap zooming
        // if (doubleTap) {
        // doubleTap = false;
        // Log.d(TAG, "doubleTap");
        // return true;
        // }

        return super.onInterceptTouchEvent(ev);
    }

    public DoubleTapWebView(Context context, Activity activity) {
        super(context, activity);
    }

    public DoubleTapWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // invalidate();
        super.onDraw(canvas);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // Log.d(TAG, "onTouchEvent");
        return super.onTouchEvent(event);
    }
}
