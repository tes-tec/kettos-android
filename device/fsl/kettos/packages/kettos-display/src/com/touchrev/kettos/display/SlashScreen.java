
package com.touchrev.kettos.display;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.SystemProperties;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher.ViewFactory;

import com.touchrev.kettos.display.help.ConnectionListener;
import com.touchrev.kettos.display.help.JNICommand;

public class SlashScreen extends Activity implements ConnectionListener, ViewFactory {
    private final static String TAG = "SlashScreen";
    public static final String SLASH_SCREEN_START_UP = "com.chaowen.action.SLASH_SCREEN_START_UP";
    private final static int SLASH_DELAY = 5;
    public final static String CANBACK = "canBack";
    private RelativeLayout slashScreen;
    private RelativeLayout noConnection;
    private Button setupButton;
    private Handler handler;
    private DisplayEthernetManager dm;
    private boolean mIsBound = false;
    private final static int CONNECTION_STATUS_CHANGE = 0;
    private View main;

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (dm.checkNetwork()) {
                showIPInfo();
                if (KettosDisplayApplication.isStartUpDiscoverDevicesScreen()
                        || KettosDisplayApplication.getHomePage().equals("")) {
                    // show Discover Devices screen
                    Intent intent = new Intent();
                    intent.setClass(SlashScreen.this, DisplayControl.class);
                    intent.putExtra(CANBACK, false);
                    intent.putExtra(DisplayControl.PAGE_KEY, DisplayControl.DISCOVER_DEVICES);
                    startActivity(intent);
                    finish();
                } else {
                    // show home page
                    Intent intent = new Intent();
                    intent.setClass(SlashScreen.this, Browser.class);
                    intent.putExtra(Browser.URL, KettosDisplayApplication.getHomePage());
                    startActivity(intent);
                    finish();
                }
            } else {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        slashScreen.setVisibility(View.GONE);
                        noConnection.setVisibility(View.VISIBLE);
                    }
                });
            }
        }
    };

    private void showIPInfo() {
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        slashScreen.setVisibility(View.VISIBLE);
        noConnection.setVisibility(View.GONE);
        Log.d(TAG, "onNewIntent");
        if (Intent.ACTION_MAIN.equals(intent.getAction())) {
            boolean alreadyOnHome = ((intent.getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
            Log.d(TAG, "alreadyOnHome:" + alreadyOnHome);
            showHome();
        }
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_MENU) {
            // super.openOptionsMenu();
        }
        return true;
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy");
        doUnbindService();
        super.onDestroy();
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            Log.d(TAG, "onServiceConnected");
            dm = ((DisplayEthernetManager.DisplayEthernetManagerBinder) service).getService();
            dm.registPlugIn(SlashScreen.this);
            Log.d(TAG, "dm connected");
            showHome();
        }

        public void onServiceDisconnected(ComponentName className) {
            dm = null;
            Log.d(TAG, "dm disconnected");
        }

    };

    private void doBindService() {
        bindService(new Intent(this, DisplayEthernetManager.class), mConnection,
                Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    private void doUnbindService() {
        dm.unRegistPlugIn(this);
        if (mIsBound) {
            unbindService(mConnection);
            mIsBound = false;
        }
    }

    @Override
    public View makeView() {
        TextView textView = new TextView(this);
        textView.setTextSize(36);
        return textView;
    }

    private void showHome() {
        if (SystemProperties.get("persist.use.show_navbar").equals("true"))
            return;
        final Thread t = new Thread(runnable);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                t.start();
            }
        }, SLASH_DELAY * 1000);
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause");
        // JNICommand.runCommand("setprop persist.use.navbar true");
        super.onPause();
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume");
        // main.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        // JNICommand.runCommand("setprop persist.use.navbar false");

        if (SystemProperties.get("persist.use.show_navbar").equals("true"))
            new AlertDialog.Builder(SlashScreen.this).setTitle("Switch to SLATE?")
                    .setMessage("This will take a reboot!")
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            SlashScreen.this.finish();
                        }
                    }).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @SuppressWarnings("deprecation")
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                            PackageManager pm = getPackageManager();
                            IntentFilter filter = new IntentFilter();

                            filter.addAction("android.intent.action.MAIN");
                            filter.addCategory("android.intent.category.HOME");
                            filter.addCategory("android.intent.category.DEFAULT");

                            ComponentName component = new ComponentName(
                                    "com.touchrev.kettos.display", ".SlashScreen");
                            ComponentName[] components = new ComponentName[] {
                                    new ComponentName("com.android.launcher",
                                            "com.android.launcher2.Launcher"), component
                            };

                            pm.clearPackagePreferredActivities("com.android.launcher");
                            pm.addPreferredActivity(filter, IntentFilter.MATCH_CATEGORY_EMPTY,
                                    components, component);

                            JNICommand.runCommand("setprop persist.use.show_navbar false");
                            JNICommand.runCommand("sync");
                            JNICommand.runCommand("reboot");
                        }
                    }).setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            // main.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
                            SlashScreen.this.finish();
                        }
                    }).create().show();

        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        if (KettosDisplayApplication.getHomePage().equals("")
                || KettosDisplayApplication.getHomePage().equals("http://[about:blank]")) {
            KettosDisplayApplication.setDefaultHomePage("http://192.168.92.10/slate/index.html");
        }
        Intent intent = new Intent();
        intent.setAction(SLASH_SCREEN_START_UP);
        sendBroadcast(intent);

        main = View.inflate(this, R.layout.slash_screen, null);
        // main.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        setContentView(main);

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case CONNECTION_STATUS_CHANGE:
                        TextView info = (TextView) findViewById(R.id.slash_screen_info);
                        if (dm.checkNetwork()) {
                            Log.d(TAG, "text_have_network_connection");
                            info.setText(getResources().getString(
                                    R.string.text_have_network_connection));
                        } else {
                            Log.d(TAG, "text_no_network_connection");
                            info.setText(getResources().getString(
                                    R.string.text_no_network_connection));
                        }
                        break;
                    default:
                        break;
                }
            }
        };
        setupButton = (Button) findViewById(R.id.button_display_setup);
        slashScreen = (RelativeLayout) findViewById(R.id.slash_screen);
        noConnection = (RelativeLayout) findViewById(R.id.slash_no_connection);
        doBindService();
        setupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(SlashScreen.this, DisplayControl.class);
                intent.putExtra(CANBACK, true);
                intent.putExtra(DisplayControl.PAGE_KEY, DisplayControl.MAIN);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onEtherNetChange(boolean plugIn) {
        Log.d(TAG, "onEtherNetChange:" + plugIn);
        if (dm != null) {
            Message m = handler.obtainMessage();
            m.setTarget(handler);
            m.what = CONNECTION_STATUS_CHANGE;
            m.sendToTarget();
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
