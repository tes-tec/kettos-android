
package com.touchrev.kettos.display;

import com.touchrev.kettos.display.help.BrowserDBHelper;
import com.touchrev.kettos.display.help.ScannerDBHelper;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class KettosDisplayApplication extends Application {
    private static SharedPreferences sp;
    private static Editor editor;
    private static String sp_name = "kettos";
    public static String browsingUrl = null;
    private static BrowserDBHelper browserDBHelper = null;
    private static ScannerDBHelper scannerDBhelper = null;
    public static final String KETTOS_SCANNER_FROM = "KettosScannerFrom";
    public static final String KETTOS_SCANNER_TO = "KettosScannerTo";
    public static final String KETTOS_SCANNER_NETMASK = "KettosScannerNetMask";

    public static String getKettosScannerFrom() {
        return sp.getString(KETTOS_SCANNER_FROM, null);
    }

    public static void setKettosScannerFrom(String from) {
        editor = sp.edit();
        editor.putString(KETTOS_SCANNER_FROM, from);
        editor.commit();
    }

    public static String getKettosScannerTo() {
        return sp.getString(KETTOS_SCANNER_TO, null);
    }

    public static void setKettosScannerTo(String to) {
        editor = sp.edit();
        editor.putString(KETTOS_SCANNER_TO, to);
        editor.commit();
    }

    public static String getKettosScannerNetMask() {
        return sp.getString(KETTOS_SCANNER_NETMASK, null);
    }

    public static void setKettosScannerNetMask(String netmask) {
        editor = sp.edit();
        editor.putString(KETTOS_SCANNER_NETMASK, netmask);
        editor.commit();
    }

    public static String getStaticIP() {
        return sp.getString("StaticIP", "");
    }

    public static void setStaticIP(String staticIP) {
        editor = sp.edit();
        editor.putString("StaticIP", staticIP);
        editor.commit();
    }

    public static String getStaticMask() {
        return sp.getString("StaticMask", "");
    }

    public static void setStaticMask(String staticMask) {
        editor = sp.edit();
        editor.putString("StaticMask", staticMask);
        editor.commit();
    }

    public static String getStaticDNS() {
        return sp.getString("StaticDNS", "");
    }

    public static void setStaticDNS(String staticDNS) {
        editor = sp.edit();
        editor.putString("StaticDNS", staticDNS);
        editor.commit();
    }

    public static String getStaticRouter() {
        return sp.getString("StaticRouter", "");
    }

    public static void setStaticRouter(String staticRouter) {
        editor = sp.edit();
        editor.putString("StaticRouter", staticRouter);
        editor.commit();
    }

    public static BrowserDBHelper getBrowserDBHelper() {
        return browserDBHelper;
    }

    public static ScannerDBHelper getScannerDBhelper() {
        return scannerDBhelper;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sp = getSharedPreferences(sp_name, Context.MODE_PRIVATE);
        browserDBHelper = new BrowserDBHelper(this);
        scannerDBhelper = new ScannerDBHelper(this);
    }

    public static String getHomePage() {
        return sp.getString("HomePage", "");
    }

    public static void setHomePage(String homePage) {
        editor = sp.edit();
        editor.putString("HomePage", homePage);
        editor.commit();
        editor = sp.edit();
        editor.putBoolean("startUpDiscoverDevicesScreen", false);
        editor.commit();
    }

    public static void setDefaultHomePage(String homePage) {
        editor = sp.edit();
        editor.putString("HomePage", homePage);
        editor.commit();
    }

    public static void notStartUpDiscoverDevicesScreen(){
    	editor = sp.edit();
        editor.putBoolean("startUpDiscoverDevicesScreen", false);
        editor.commit();
    }

    public static boolean isUseDHCP() {
        return sp.getBoolean("useDHCP", false);
    }

    public static void setUseDHCP(boolean useDHCP) {
        editor = sp.edit();
        editor.putBoolean("useDHCP", useDHCP);
        editor.commit();
    }

    public static boolean isStartUpDiscoverDevicesScreen() {
        return sp.getBoolean("startUpDiscoverDevicesScreen", false);
    }

    public static void setStartUpDiscoverDevicesScreen(boolean startUpDiscoverDevicesScreen) {
        editor = sp.edit();
        editor.putBoolean("startUpDiscoverDevicesScreen", startUpDiscoverDevicesScreen);
        editor.commit();
	/*
        editor = sp.edit();
        editor.putString("HomePage", "");
        editor.commit();
	*/
    }
}
