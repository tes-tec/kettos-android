package com.touchrev.kettos.display.help;


public class JNICommand {

    public static native boolean runCommand(String cmd);
    
    public static native String runCommand2(String cmd);
    
    public static native String runCommand3(String cmd);

    public static native void listenFile(String fileName, CarrierListener callBack);
    
    public final static int IN_ACCESS = 0x00000001; /* File was accessed */
    public final static int IN_MODIFY = 0x00000002; /* File was modified */
    public final static int IN_ATTRIB = 0x00000004; /* Metadata changed */
    public final static int IN_CLOSE_WRITE = 0x00000008; /* Writtable file was closed */
    public final static int IN_CLOSE_NOWRITE = 0x00000010; /* Unwrittable file closed */
    public final static int IN_OPEN = 0x00000020; /* File was opened */
    public final static int IN_MOVED_FROM = 0x00000040; /* File was moved from X */
    public final static int IN_MOVED_TO = 0x00000080; /* File was moved to Y */
    public final static int IN_CREATE = 0x00000100; /* Subfile was created */
    public final static int IN_DELETE = 0x00000200; /* Subfile was deleted */
    public final static int IN_DELETE_SELF = 0x00000400; /* Self was deleted */
    public final static int IN_MOVE_SELF = 0x00000800; /* Self was moved */

    /* the following are legal events. they are sent as needed to any watch */
    public final static int IN_UNMOUNT = 0x00002000; /* Backing fs was unmounted */
    public final static int IN_Q_OVERFLOW = 0x00004000; /* Event queued overflowed */
    public final static int IN_IGNORED = 0x00008000; /* File was ignored */
    public final static int IN_RENAME = 0x00020000; /* rename */

    /* helper events */
    public final static int IN_CLOSE = (IN_CLOSE_WRITE | IN_CLOSE_NOWRITE); /* close */
    public final static int IN_MOVE = (IN_MOVED_FROM | IN_MOVED_TO); /* moves */

    /* special flags */
    public final static int IN_ISDIR = 0x40000000;
    
    /*
    * event occurred against
    * dir
    */
    public final static int IN_ONESHOT = 0x80000000; /* only send event once */
    
    /*
    * All of the events - we build the list by hand so that we can add flags in
    * the future and not break backward compatibility. Apps will get only the
    * events that they originally wanted. Be sure to add new events here!
    */
    public final static int IN_ALL_EVENT = (IN_ACCESS | IN_MODIFY | IN_ATTRIB | IN_CLOSE_WRITE | IN_MOVE_SELF
    | IN_CLOSE_NOWRITE | IN_OPEN | IN_MOVED_FROM | IN_MOVED_TO | IN_DELETE | IN_CREATE | IN_DELETE_SELF);
    
    public static native int inotifyInit();
    
    public static native int inotifyRead(int fd,int wd,CarrierListener callBack);
    
    public static native int inotifyAddWatch(int fd,String path,int mask);
    
    public static native int inotifyRemoveWatch(int wd);
    
    static {
        System.loadLibrary("com_jni-kettos");
    }
}
