package com.touchrev.kettos.display.help;

public interface MonitorListener {
    void onEtherNetPlug(boolean plugIn);
}
