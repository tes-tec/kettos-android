package com.touchrev.kettos.display.help;

public interface CarrierListener {
    void onCarrierChange();
    void onMonitorFail();
    void onMointorStop();
}
