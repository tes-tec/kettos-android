
package com.touchrev.kettos.display.help;

import android.os.Handler.Callback;
import android.os.HandlerThread;
import android.os.Message;

public class JNIHanderThread extends HandlerThread implements Callback {

    public static final String TAG = "JNIHanderThread";
    public static final String JNI_COMM = "comm";
    public static final String JNI_TYPE = "type";
    public static final int JNI_TYPE1 = 1;
    public static final int JNI_TYPE2 = 2;

    public JNIHanderThread(String name) {
        super(name);
    }

    @Override
    public boolean handleMessage(Message msg) {
        return false;
    }
}
