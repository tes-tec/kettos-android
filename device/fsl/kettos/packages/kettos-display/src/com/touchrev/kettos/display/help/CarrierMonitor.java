
package com.touchrev.kettos.display.help;

import android.content.Context;
import android.util.Log;

import com.touchrev.kettos.display.DisplayEthernetManager;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CarrierMonitor implements CarrierListener {
    private static final String TAG = "CarrierMonitor";
    private static String shellPath = "/ip_monitor.sh";
    private static String carrierPath = "/carrier";
    private static String carrierPIDPath = "/carrier_PID";
    private MonitorListener monitorListener;
    private boolean stop;

    // private Thread shellThread;
    // private Thread monitorThread;

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            Log.v(TAG, "read:" + buffer);
            Log.v(TAG, "size:" + read);
            out.write(buffer, 0, read);
        }
    }

    private String readLine(String path) throws IOException {
        FileReader fr = new FileReader(path);
        BufferedReader br = new BufferedReader(fr);
        String res = br.readLine();
        br.close();
        fr.close();
        Log.d(TAG, "read line:" + res);
        return res;
    }

    public CarrierMonitor(Context context, final MonitorListener monitorListener) {
        shellPath = context.getFilesDir().getAbsolutePath() + shellPath;
        carrierPath = context.getFilesDir().getAbsolutePath() + carrierPath;
        carrierPIDPath = context.getFilesDir().getAbsolutePath() + carrierPIDPath;
    }

    private boolean checkWire() {
        String res = "";
        Log.d(TAG, android.os.Build.MODEL);
        if ("WANDBOARD".equals(android.os.Build.MODEL)) {
            res = JNICommand.runCommand2("cat /sys/devices/platform/enet.0/net/eth0/carrier");
        } else if ("TDM3730".equals(android.os.Build.MODEL)) {
            res = JNICommand.runCommand2("cat /sys/devices/platform/smsc911x/net/eth0/carrier");
        }
        if ("1\n".equals(res)) {
            Log.d(TAG, "check wire connected");
            return true;
        } else {
            Log.d(TAG, "check wire not connected");
            return false;
        }
    }

    public boolean isPlugIn() throws IOException {
        Log.d(TAG, "isPlugIn");
        boolean isIn = false;
        String res = readLine(carrierPath);
        if (res != null) {
            Log.d(TAG, "carrierPath:" + res);
            String regEx = "got[\\S\\s]+\\sstate\\sUP";
            Pattern pat = Pattern.compile(regEx);
            Matcher mat = pat.matcher(res);
            boolean rs = mat.find();
            if (rs) {
                isIn = true;
            }

            String regEx2 = "got[\\S\\s]+\\sstate\\sDOWN";
            Pattern pat2 = Pattern.compile(regEx2);
            Matcher mat2 = pat2.matcher(res);
            boolean rs2 = mat2.find();
            if (rs2) {
                isIn = false;
            }
            Log.d(TAG, "isPlugIn:" + isIn);
        }
        return isIn;
    }

    @Override
    public void onCarrierChange() {
        Log.d(TAG, "onCarrierChange");
        try {
            boolean res = isPlugIn();
            if (res != DisplayEthernetManager.isCarrierIn)
                monitorListener.onEtherNetPlug(res);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMonitorFail() {
        Log.d(TAG, "onMonitorFail");
    }

    @Override
    public void onMointorStop() {
        Log.d(TAG, "onMointorStop");
    }
}
