#!/system/bin/sh
echo $$ > $1_PID
log -p d -t JNICOMMAND "ip_monitor_in $1"
ip monitor | while read line; do
        log -p d -t JNICOMMAND "ip_monitor_while"
        tt=`echo $line | busybox grep state`
        if [ -n "$tt" ]
        then
                echo got $line > $1;
        fi
done
log -p d -t JNICOMMAND "ip_monitor_out"
