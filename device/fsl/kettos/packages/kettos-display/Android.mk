#
# Copyright (C) 2008 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
LOCAL_PATH := $(call my-dir)
#LIB_PATH := ../crosswalk-webview-7.36.154.6-arm
LIB_PATH := ../crosswalk-webview-7-arm
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

LOCAL_STATIC_JAVA_LIBRARIES := android-common \
	android-support-v4 \
	xwalk_core_library_java

#$(shell cp $(wildcard $(LOCAL_PATH)/$(LIB_PATH)/libs/armeabi-v7a/*.so $(TARGET_OUT_INTERMEDIATE_LIBRARIES)))

LOCAL_JNI_SHARED_LIBRARIES := libcom_jni-kettos \
	libxwalkcore

LOCAL_SRC_FILES := $(call all-subdir-java-files)

LOCAL_RESOURCE_DIR := $(LOCAL_PATH)/res \
	$(LOCAL_PATH)/$(LIB_PATH)/res
LOCAL_AAPT_FLAGS := --auto-add-overlay \
    --extra-packages org.xwalk.core

LOCAL_PACKAGE_NAME := SLATEDisplay
LOCAL_CERTIFICATE := platform
LOCAL_PROGUARD_ENABLED := disabled
LOCAL_OVERRIDES_PACKAGES := Home

include $(BUILD_PACKAGE)

#####################################################################

include $(CLEAR_VARS)
LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES := xwalk_core_library_java:$(LIB_PATH)/libs/xwalk_core_library_java.jar

include $(BUILD_MULTI_PREBUILT)

#####################################################################

include $(call all-makefiles-under,$(LOCAL_PATH))
