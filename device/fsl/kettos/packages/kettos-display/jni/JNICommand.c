/*
 * Copyright 2009 Cedric Priscal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <termios.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <jni.h>
#include <stdio.h>
#include <android/log.h>
#include <sys/inotify.h>
#include <unistd.h>
#include "com_touchrev_kettos_display_help_JNICommand.h"

#define LOG_TAG "JNICOMMAND"

#define LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)
#define LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define LOGW(...)  __android_log_print(ANDROID_LOG_WARN,LOG_TAG,__VA_ARGS__)
#define LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)
#define LOGF(...)  __android_log_print(ANDROID_LOG_FATAL,LOG_TAG,__VA_ARGS__)

JNIEXPORT jboolean JNICALL Java_com_touchrev_kettos_display_help_JNICommand_runCommand(
		JNIEnv * env, jclass thiz, jstring cmd) {
	LOGD("=====runCommand in=====");
	char su_cmd[1024];

	const char *cmd_utf = (*env)->GetStringUTFChars(env, cmd, NULL);
	sprintf(su_cmd, "su0 -c \"%s\"", cmd_utf);
	(*env)->ReleaseStringUTFChars(env, cmd, cmd_utf);

	LOGD("Running command : %s", su_cmd);
	int ret = system(su_cmd);
	LOGD("system() ret = %d", ret);
	LOGD("=====runCommand out=====");
	return (ret == 0) ? JNI_TRUE : JNI_FALSE;
}

JNIEXPORT jstring JNICALL Java_com_touchrev_kettos_display_help_JNICommand_runCommand2(
		JNIEnv * env, jclass thiz, jstring cmd) {
	LOGD("=====runCommand2 in=====");
	char su_cmd[1024];

	const char *cmd_utf = (*env)->GetStringUTFChars(env, cmd, NULL);
	sprintf(su_cmd, "su0 -c \"%s\"", cmd_utf);
	(*env)->ReleaseStringUTFChars(env, cmd, cmd_utf);

	LOGD("Running command2 : %s", su_cmd);
	FILE *fpipe;
	char line[256];
	char result[1024];
	memset(result, 0, sizeof result);
	if (!(fpipe = (FILE*) popen(su_cmd, "r"))) {
		LOGD("=====runCommand2 out:popen=====");
		return NULL;
	}
	LOGD("after popen");
	while (fgets(line, sizeof line, fpipe)) {
		LOGD("get2 line: %s", line);
		strcat(result, line);
		memset(line, 0, sizeof line);
	}
	pclose(fpipe);
	LOGD("get2 result: %s", result);
	if (!IsUTF8(result, strlen(result))) {
		LOGD("=====runCommand2 out:utf8error=====");
		return NULL;
	}
	LOGD("=====runCommand2 out=====");
	return (*env)->NewStringUTF(env, result);
}

JNIEXPORT jstring JNICALL Java_com_touchrev_kettos_display_help_JNICommand_runCommand3(
		JNIEnv * env, jclass thiz, jstring cmd) {
	pid_t pid;
	pid = fork();
	if (pid < 0) {
		LOGD("fork failed");
		return (*env)->NewStringUTF(env, "fork failed...");
	} else if (pid == 0) {
		LOGD("child process");
		char su_cmd[1024];
		const char *cmd_utf = (*env)->GetStringUTFChars(env, cmd, NULL);
		sprintf(su_cmd, "su0 -c \"%s\"", cmd_utf);
		(*env)->ReleaseStringUTFChars(env, cmd, cmd_utf);

		LOGD("Running command3 : %s", su_cmd);
		FILE *fpipe;
		char line[256];
		char result[1024];
		memset(result, 0, sizeof result);
		if (!(fpipe = (FILE*) popen(su_cmd, "r"))) {
			return NULL;
		}
		LOGD("after3 popen");
		while (fgets(line, sizeof line, fpipe)) {
			LOGD("get3 line: %s", line);
			strcat(result, line);
			memset(line, 0, sizeof line);
		}
		pclose(fpipe);
		LOGD("get3 result: %s", result);
		if (!IsUTF8(result, strlen(result))) {
			return NULL;
		}
		return (*env)->NewStringUTF(env, result);
	}else{
		LOGD("parent process");
		return (*env)->NewStringUTF(env, "run command in new process...");
	}
}

int IsUTF8(const void* pBuffer, long size) {
	int IsUTF8 = 1;
	unsigned char* start = (unsigned char*) pBuffer;
	unsigned char* end = (unsigned char*) pBuffer + size;
	while (start < end) {
		if (*start < 0x80) {
			start++;
		} else if (*start < (0xC0)) {
			IsUTF8 = 0;
			break;
		} else if (*start < (0xE0)) {
			if (start >= end - 1)
				break;
			if ((start[1] & (0xC0)) != 0x80) {
				IsUTF8 = 0;
				break;
			}
			start += 2;
		} else if (*start < (0xF0)) {
			if (start >= end - 2)
				break;
			if ((start[1] & (0xC0)) != 0x80 || (start[2] & (0xC0)) != 0x80) {
				IsUTF8 = 0;
				break;
			}
			start += 3;
		} else {
			IsUTF8 = 0;
			break;
		}
	}
	return IsUTF8;
}

JNIEXPORT void JNICALL Java_com_touchrev_kettos_display_help_JNICommand_listenFile(
		JNIEnv *env, jclass thiz, jstring fileName, jobject callBack) {

#define EVENT_NUM 16
#define MAX_BUF_SIZE 1024
	char pathName[1024];
	int fd, wd;
	char buffer[MAX_BUF_SIZE + 1];
	char * offset = NULL;
	struct inotify_event * event;
	int i, len, tmp_len;
	char strbuf[16];
	char * event_array[] = { "File was accessed", "File was modified",
			"File attributes were changed", "writtable file closed",
			"Unwrittable file closed", "File was opened",
			"File was moved from X", "File was moved to Y",
			"Subfile was created", "Subfile was deleted", "Self was deleted",
			"Self was moved", "", "Backing fs was unmounted",
			"Event queued overflowed", "File was ignored" };

	LOGD("in listenFile");
	if ((fd = inotify_init()) < 0) {
		LOGD("inotify_init error");
		return;
	}
	LOGD("inotify_init done");

	const char *s = (*env)->GetStringUTFChars(env, fileName, NULL);
	LOGD("GetStringUTFChars done");
	sprintf(pathName, "%s", s);
	LOGD("sprintf done");
	(*env)->ReleaseStringUTFChars(env, fileName, s);
	LOGD("ReleaseStringUTFChars done");
	LOGD("pathName: %s", pathName);

	if ((wd = inotify_add_watch(fd, pathName, IN_ALL_EVENTS)) < 0) {
		LOGD("inotify_add_watch error");
		return;
	}

	LOGD("inotify_add_watch done");

	while (len = read(fd, buffer, MAX_BUF_SIZE)) {
		LOGD("read: %d", len);
		offset = buffer;
		event = (struct inotify_event *) buffer;
		while (((char *) event - buffer) < len) {
			LOGD(
					"Object type: %s\n", event->mask & IN_ISDIR ? "Direcotory" : "File");
			if (event->wd != wd) {
				LOGD("wd not match!");
				continue;
			}
			LOGD("Object name: %s\n", event->name);
			LOGD("Event mask: %08X\n", event->mask);
			for (i = 0; i < EVENT_NUM; i++) {
				if (event_array[i][0] == '\0')
					continue;
				if (event->mask & (1 << i)) {
					LOGD("Event: %s\n", event_array[i]);
				}
			}
			tmp_len = sizeof(struct inotify_event) + event->len;
			event = (struct inotify_event *) (offset + tmp_len);
			offset += tmp_len;
		}
	}
}

JNIEXPORT jint JNICALL Java_com_touchrev_kettos_display_help_JNICommand_inotifyInit(
		JNIEnv *env, jclass thiz) {
	LOGD("inotifyInit");
	return inotify_init();
}

JNIEXPORT jint JNICALL Java_com_touchrev_kettos_display_help_JNICommand_inotifyRead(
		JNIEnv *env, jclass thiz, jint fd, jint wd, jobject call_back) {

#define EVENT_NUM 16
#define MAX_BUF_SIZE 1024
	int i, len, tmp_len;
	char * offset = NULL;
	struct inotify_event * event;
	char buffer[MAX_BUF_SIZE + 1];
	char * event_array[] = { "File was accessed", "File was modified",
			"File attributes were changed", "writtable file closed",
			"Unwrittable file closed", "File was opened",
			"File was moved from X", "File was moved to Y",
			"Subfile was created", "Subfile was deleted", "Self was deleted",
			"Self was moved", "", "Backing fs was unmounted",
			"Event queued overflowed", "File was ignored" };
	LOGD("inotifyRead");
	len = read(fd, buffer, MAX_BUF_SIZE);
	LOGD("read: %d", len);
	if (len < 0) {
		return len;
	}
	offset = buffer;
	event = (struct inotify_event *) buffer;

	jclass call_back_class = (*env)->GetObjectClass(env, call_back);
	jmethodID onCarrierChangeID = (*env)->GetMethodID(env, call_back_class,
			"onCarrierChange", "()V");
	if (onCarrierChangeID == NULL) {
		LOGD("getMethodId is failed");
	}

	while (((char *) event - buffer) < len) {
		LOGD(
				"Object type: %s\n", event->mask & IN_ISDIR ? "Direcotory" : "File");
		if (event->wd != wd) {
			LOGD("wd not match!");
			continue;
		}
		LOGD("Object name: %s\n", event->name);
		LOGD("Event mask: %08X\n", event->mask);

		for (i = 0; i < EVENT_NUM; i++) {
			if (event_array[i][0] == '\0')
				continue;
			if (event->mask & (1 << i)) {
				LOGD("Event: %s\n", event_array[i]);
			}
		}

		if (event->mask == IN_MODIFY) {
			(*env)->CallVoidMethod(env, call_back, onCarrierChangeID);
		}

		tmp_len = sizeof(struct inotify_event) + event->len;
		event = (struct inotify_event *) (offset + tmp_len);
		offset += tmp_len;
	}
	return len;
}

JNIEXPORT jint JNICALL Java_com_touchrev_kettos_display_help_JNICommand_inotifyAddWatch(
		JNIEnv *env, jclass thiz, jint fd, jstring path, jint mask) {
	const char *str;
	str = (*env)->GetStringUTFChars(env, path, NULL);

	LOGD("inotifyAddWatch mask:%08X path:%s", mask, str);
	int wd = inotify_add_watch(fd, str, mask);
	(*env)->ReleaseStringUTFChars(env, path, str);
	return wd;
}
