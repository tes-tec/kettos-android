#!/system/bin/sh

pm_install=`getprop persist.pm_install`
case "$pm_install" in
    1)  exit
    ;;
esac

pm uninstall com.touchrevolution.reliability
pm install /system/kettos/ReliabilityApp.apk_

pm uninstall com.tpk.touchtest
pm install /system/kettos/TouchTest.apk_

setprop persist.pm_install 1
